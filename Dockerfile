FROM ubuntu

RUN DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Bucharest

#TZ required for ssh, it will require DEBIAN_FRONTEND,TZ, libaio required for instantclient
RUN apt-get update \
    && apt-get install -y tzdata \
    && apt-get -y install unzip \
    && apt-get -y install libaio-dev \
    && apt-get install -y python3-pip python3-dev \
    && apt-get install -y unixodbc-dev \
    && apt-get install -y openssh-server \
    && cd /usr/local/bin \
    && ln -s /usr/bin/python3 python \
    && mkdir -p /opt/data/api \
    && cd /opt/data
#adding oracle instantclient
ADD ./oracle-instantclient/ /opt/data
ADD ./prepare-env.sh /opt/data

WORKDIR /opt/data

ENV ORACLE_HOME=/opt/oracle/instantclient
ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ORACLE_HOME

ENV OCI_HOME=/opt/oracle/instantclient
ENV OCI_LIB_DIR=/opt/oracle/instantclient
ENV OCI_INCLUDE_DIR=/opt/oracle/instantclient/sdk/include

RUN ["/bin/bash","./prepare-env.sh"]
#adding ssh key, config, known_hosts
ARG ssh_prv_key
ARG config
ARG known_hosts

RUN mkdir -p /root/.ssh && \
    chmod 0700 /root/.ssh

RUN echo "$ssh_prv_key" > /root/.ssh/id_rsa && \
    echo "$config" > /root/.ssh/config && \
    echo "$known_hosts" > /root/.ssh/known_hosts &&\
    chmod 644 /root/.ssh/known_hosts &&\
    chmod 600 /root/.ssh/id_rsa && \
    chmod 600 /root/.ssh/config

WORKDIR /usr/src/app
ENV PYTHONWARNINGS="ignore:Unverified HTTPS request"
COPY requirements.txt .
RUN pip3 install --no-cache-dir --trusted-host pypi.org --trusted-host files.pythonhosted.org --trusted-host pypi.python.org -r requirements.txt

COPY ./app/ /usr/src/app/
EXPOSE 5000

CMD [ "python", "./app.py" ]

