#!/bin/bash

CONTAINER_HOME=$(dirname `which $0`)
IMAGE_EXISTS=$(docker ps | grep -i getcustomerenv1 | wc -l)
IMAGE_NAME=getcustomerenv1
cd $CONTAINER_HOME

if [ $IMAGE_EXISTS -gt 0 ]
then 
	for container in $(docker ps | awk -v awkvarr="$IMAGE_NAME" '$2~awkvarr{print $1}'); do docker container stop $container && docker container rm $container; done
docker rmi -f $IMAGE_NAME
fi
docker build --build-arg http_proxy=http://10.140.206.118:3128 --build-arg https_proxy=https://10.140.206.118:3128 --build-arg ssh_prv_key="$(cat /opt/testauto/dexAPIs/pegachecks/ssh_keys/id_rsa)" --build-arg config="$(cat /opt/testauto/dexAPIs/pegachecks/ssh_keys/config)" --build-arg known_hosts="$(cat /opt/testauto/dexAPIs/pegachecks/ssh_keys/known_hosts)" -t "$IMAGE_NAME" . | tee -a build.log
# docker container run -d --env-file ./20.env -p 8020:5000 getcustomerenv1
# docker container run -d --env-file ./30.env -p 8030:5000 getcustomerenv1
docker-compose up -d
