create or replace package body PKG_COMMON_CUSTOMER_AUTO
----by Larentiu Faget needs to run on edwtst.
 as

  procedure Truncate_tables_comm is
  
  begin
  
    EXECUTE IMMEDIATE '
TRUNCATE TABLE STG_RT_SUBSCRIBER_30_AUTO   DROP STORAGE';
    EXECUTE IMMEDIATE '
TRUNCATE TABLE subscriber_30_AUTO          DROP STORAGE';
    EXECUTE IMMEDIATE '
TRUNCATE TABLE table_site_part_30_AUTO     DROP STORAGE';
    EXECUTE IMMEDIATE '
TRUNCATE TABLE customer_30_AUTO            DROP STORAGE';
    EXECUTE IMMEDIATE '
TRUNCATE TABLE service_agreement_30_AUTO   DROP STORAGE';
    EXECUTE IMMEDIATE '
TRUNCATE TABLE csm_offer_30_AUTO           DROP STORAGE';
    EXECUTE IMMEDIATE '
TRUNCATE TABLE STG_RT_SUBSCRIBER_20_AUTO   DROP STORAGE';
    EXECUTE IMMEDIATE '
TRUNCATE TABLE subscriber_20_AUTO          DROP STORAGE';
    EXECUTE IMMEDIATE '
TRUNCATE TABLE table_site_part_20_AUTO     DROP STORAGE';
    EXECUTE IMMEDIATE '
TRUNCATE TABLE customer_20_AUTO            DROP STORAGE';
    EXECUTE IMMEDIATE '
TRUNCATE TABLE service_agreement_20_AUTO   DROP STORAGE';
    EXECUTE IMMEDIATE '
TRUNCATE TABLE csm_offer_20_AUTO           DROP STORAGE';
    EXECUTE IMMEDIATE '
TRUNCATE TABLE rm1_resource_20_auto           DROP STORAGE';
    EXECUTE IMMEDIATE '
TRUNCATE TABLE rm1_resource_30_auto           DROP STORAGE';
    EXECUTE IMMEDIATE '
TRUNCATE TABLE vw_car_subs_20_auto           DROP STORAGE';
    EXECUTE IMMEDIATE '
TRUNCATE TABLE vw_car_subs_30_auto           DROP STORAGE';
  
  END Truncate_tables_comm;

  procedure Copy_table_30 is
  
    links varchar2(50);
  
  begin
    EXECUTE IMMEDIATE '
INSERT /*+ APPEND */
    INTO vw_car_subs_30_auto
SELECT  * FROM vw_car_subs@PEGA_UAT3_AA e ';
  
    EXECUTE IMMEDIATE 'COMMIT';
  
    EXECUTE IMMEDIATE '
INSERT /*+ APPEND */
    INTO rm1_resource_30_auto
SELECT  * FROM rm1_resource@Cm30.world e ';
  
    EXECUTE IMMEDIATE 'COMMIT';
  
    EXECUTE IMMEDIATE '
INSERT /*+ APPEND */
    INTO STG_RT_SUBSCRIBER_30_AUTO
SELECT  * FROM RT_AREA.STG_RT_SUBSCRIBER@PEGA_UAT3 e ';
  
    EXECUTE IMMEDIATE 'COMMIT';
  
    EXECUTE IMMEDIATE '
INSERT /*+ APPEND */
    INTO subscriber_30_AUTO
select * from subscriber@CM30.WORLD a ';
  
    EXECUTE IMMEDIATE 'COMMIT';
  
    EXECUTE IMMEDIATE '
INSERT /*+ APPEND */ INTO table_site_part_30_AUTO
select * from table_site_part@SA_UAT2.WORLD         c
';
  --WHERE c.instance_name = ''CBU Subscription'' 
    EXECUTE IMMEDIATE 'COMMIT';
  
    EXECUTE IMMEDIATE '
INSERT /*+ APPEND */ INTO customer_30_AUTO
select * from  customer@CM30.WORLD                 f';
  
    EXECUTE IMMEDIATE 'COMMIT';
  
    EXECUTE IMMEDIATE '
INSERT /*+ APPEND */ INTO service_agreement_30_AUTO
select * from service_agreement@Cm30.world        g ';
  
    EXECUTE IMMEDIATE 'COMMIT';
  
    EXECUTE IMMEDIATE '
INSERT /*+ APPEND */ INTO csm_offer_30_AUTO
select * from csm_offer@cm30.world                i ';
  
    EXECUTE IMMEDIATE 'COMMIT';
  
    for links in (select db_link from v$dblink) loop
    
      DBMS_SESSION.CLOSE_DATABASE_LINK(links.db_link);
    end loop;
  
  END Copy_table_30;

  procedure Copy_table_20 is
  
    links varchar2(50);
  
  begin
  
    EXECUTE IMMEDIATE '
INSERT /*+ APPEND */
    INTO vw_car_subs_20_auto
SELECT  * FROM APPLICATION_AREA.vw_car_subs@PEGA_UAT1_AREA e ';
  
    EXECUTE IMMEDIATE 'COMMIT';
  
    EXECUTE IMMEDIATE '
INSERT /*+ APPEND */
    INTO rm1_resource_20_auto
SELECT  * FROM rm1_resource@CM20.new e ';
  
    EXECUTE IMMEDIATE 'COMMIT';
  
    EXECUTE IMMEDIATE '
INSERT /*+ APPEND */ INTO STG_RT_SUBSCRIBER_20_AUTO
SELECT  * FROM RT_AREA.STG_RT_SUBSCRIBER@PEGA_UAT1_AREA e';
  
    EXECUTE IMMEDIATE 'COMMIT';
  
    EXECUTE IMMEDIATE '
INSERT /*+ APPEND */ INTO subscriber_20_AUTO
select * from subscriber@cm20.new                 a';
  
    EXECUTE IMMEDIATE 'COMMIT';
  
    EXECUTE IMMEDIATE '
INSERT /*+ APPEND */ INTO table_site_part_20_AUTO
select * from table_site_part@SA.WORLD            c ';
  
    EXECUTE IMMEDIATE 'COMMIT';
  
    EXECUTE IMMEDIATE '
INSERT /*+ APPEND */ INTO customer_20_AUTO
select * from  customer@CM20.new                 f ';
  
    EXECUTE IMMEDIATE 'COMMIT';
  
    EXECUTE IMMEDIATE '
INSERT /*+ APPEND */ INTO service_agreement_20_AUTO
select * from service_agreement@cm20.new          g ';
  
    EXECUTE IMMEDIATE 'COMMIT';
  
    EXECUTE IMMEDIATE '
INSERT /*+ APPEND */ INTO csm_offer_20_AUTO
select * from csm_offer@cm20.new                  i ';
  
    EXECUTE IMMEDIATE 'COMMIT';
  
    for links in (select db_link from v$dblink) loop
    
      DBMS_SESSION.CLOSE_DATABASE_LINK(links.db_link);
    end loop;
  
  END Copy_table_20;

  PROCEDURE common_customer_dg IS
    v_count_20 varchar2(20);
    v_count_30 varchar2(20);
  
  BEGIN
  
    FOR common_cur IN (select distinct a.prim_resource_val as NR_CM,
                                       a.subscriber_no AS SUBSCRIBER_CM,
                                       a.sub_status as STATUS_CM,
                                       (to_date(to_char(SYSDATE, 'yyyy-MM'),
                                                'yyyy-MM') -
                                       to_date(to_char(F.OPEN_DATE,
                                                        'yyyy-MM'),
                                                'yyyy-MM')) / 31 MONTH,
                                       e.prim_res_value as NR_PEGA,
                                       e.subs_key as SUBSCRIBER_PEGA,
                                       e.sub_status as STATUS_PEGA
                         from STG_RT_SUBSCRIBER_30_AUTO e,
                              subscriber_30_AUTO        a,
                              customer_30_AUTO          f
                        WHERE a.prim_resource_val =
                              substr(e.prim_res_value, 3, 10)
                          and a.customer_id = F.customer_id
                          and a.sub_status = 'A'
                          and e.subs_profile = 'Postpaid'
                          and a.prim_resource_val like '7%'
                          and length(a.prim_resource_val) = 9
                          AND NOT EXISTS
                        (SELECT h.NR_CM
                                 FROM table_common_customer_AUTO h
                                WHERE a.prim_resource_val = h.NR_CM)) LOOP
    
      INSERT /*+ APPEND */
      INTO table_common_customer_AUTO
        (NR_CM,
         SUBSCRIBER_CM,
         STATUS_CM,
         MONTH,
         NR_PEGA,
         SUBSCRIBER_PEGA,
         STATUS_PEGA,
         ENV)
      values
        (common_cur.NR_CM,
         common_cur.subscriber_cm,
         common_cur.status_cm,
         common_cur.MONTH,
         common_cur.nr_pega,
         common_cur.SUBSCRIBER_PEGA,
         common_cur.STATUS_PEGA,
         '30');
    
      COMMIT;
    
    END LOOP;
  
    --20
    FOR common_cur IN (select distinct a.prim_resource_val as NR_CM,
                                       a.subscriber_no AS SUBSCRIBER_CM,
                                       a.sub_status as STATUS_CM,
                                       (to_date(to_char(SYSDATE, 'yyyy-MM'),
                                                'yyyy-MM') -
                                       to_date(to_char(F.OPEN_DATE,
                                                        'yyyy-MM'),
                                                'yyyy-MM')) / 31 MONTH,
                                       e.prim_res_value as NR_PEGA,
                                       e.subs_key as SUBSCRIBER_PEGA,
                                       e.sub_status as STATUS_PEGA
                         from STG_RT_SUBSCRIBER_20_AUTO e,
                              subscriber_20_AUTO        a,
                              customer_20_AUTO          f
                        WHERE a.prim_resource_val =
                              substr(e.prim_res_value, 3, 10)
                          and a.customer_id = F.customer_id
                          and a.sub_status = 'A'
                          and e.subs_profile = 'Postpaid'
                          and a.prim_resource_val like '7%'
                          and length(a.prim_resource_val) = 9
                          AND NOT EXISTS
                        (SELECT h.NR_CM
                                 FROM table_common_customer_AUTO h
                                WHERE a.prim_resource_val = h.NR_CM)) LOOP
      INSERT /*+ APPEND */
      INTO table_common_customer_AUTO
        (NR_CM,
         SUBSCRIBER_CM,
         STATUS_CM,
         MONTH,
         NR_PEGA,
         SUBSCRIBER_PEGA,
         STATUS_PEGA,
         ENV)
      values
        (common_cur.NR_CM,
         common_cur.subscriber_cm,
         common_cur.status_cm,
         common_cur.MONTH,
         common_cur.nr_pega,
         common_cur.SUBSCRIBER_PEGA,
         common_cur.STATUS_PEGA,
         '20');
    
      COMMIT;
    
    END LOOP;
  
    select count(*)
      into v_count_20
      from table_common_customer_AUTO
     WHERE INSERT_DATE < sysdate
       and env = '20';
  
    select count(*)
      into v_count_30
      from table_common_customer_AUTO
     WHERE INSERT_DATE < sysdate
       and env = '30';
  
    -- Afisare
    dbms_output.put_line('######### DONE #########');
    dbms_output.put_line('Inserted in table_common_customer with env 20: ' ||
                         v_count_20);
    dbms_output.put_line('Inserted in table_common_customer with env 30: ' ||
                         v_count_30);
    dbms_output.put_line('######### EXIT #########');
  
  END common_customer_dg;

  procedure call_common_cust is
  
  begin
  
    Truncate_tables_comm;
    Copy_table_30;
    Copy_table_20;
    common_customer_dg;
  
  END call_common_cust;

END PKG_COMMON_CUSTOMER_AUTO;
