from flask import Flask, request, make_response
from flask_restful import Resource, Api, reqparse
from flask_cors import CORS
from scratch import Subscribers
from databasecall import DatabaseCall
from sshcli import SSH
import logging
from flask import jsonify
import os
import random
from rejson import Client, Path
import json
import re
import ast
import requests
import getPegaOffers
import executionDetails
import redis
from generateSSN import SSNGenerator
from prerequisites_pkg.getPrereq import getPrerequisites
from getCaponeStatus import CaponeActions as CA
import get_Customer
from get_migratedCust import query_Builder, get_migratedCustParams
from prerequisites_pkg import get_username
from get_deviceId import device_id
from connect_to_db import database_Connection
import clean_Sim
import clean_msisdn
import getlogfile
import get_device_IMEI
from generatePPBVoucher import voucherAction

app = Flask(__name__)
app.config['JSONIFY_PRETTYPRINT_REGULAR'] = True
CORS(app)
api = Api(app)

logging.basicConfig(
    format='%(asctime)s %(levelname)-8s %(message)s',
    level=logging.INFO,
    datefmt='%Y-%m-%d %H:%M:%S')
env_db_query=os.environ['ENV']
env_token_host = os.environ['ENV_TOKEN_HOST']
#subskeys = DatabaseCall(100,'024_Achizitie_Franciza_MNP_PREPAY_SERVICES','dummy',env_db_query).getSubsList()
accepted_store = DatabaseCall(10,'024_Achizitie_Franciza_MNP_PREPAY_SERVICES','store',env_db_query).getPosibleMethods()
accepted_franciza = DatabaseCall(10,'024_Achizitie_Franciza_MNP_PREPAY_SERVICES','franciza',env_db_query).getPosibleMethods()
subs_tuple = DatabaseCall(800,'022_Franciza_MNP_PREPAY_MOBILE_WITH_DISCFMC','store',env_db_query).getSubsListTuple()
prepaid_list = DatabaseCall(800,'022_Franciza_MNP_PREPAY_MOBILE_WITH_DISCFMC','store',env_db_query).getPrepaid()
rj = Client(host='redis', port=6379, decode_responses=True)
r_conn = redis.Redis(host='redis')

@app.route('/checkVoucherData/<string:voucherSerialNumber>')
def checkVoucherData(vsn: str):
    response = voucherAction(url='http://bv-eai-uat4:7810/eai/services/SRSService', voucherSerialNumber=vsn).checkVoucherData()
    return jsonify({'Result': response})

def getAcceptedMethods(chnl):
    if chnl == 'store':
        accepted_method = accepted_store
    if chnl == 'franciza':
        accepted_method = accepted_franciza
    return accepted_method

def testMethodType(in_method,canal):
    if in_method not in getAcceptedMethods(canal):
        ret_str = in_method + " must be in " + str(getAcceptedMethods(canal))
    else:
        ret_str = "OK"
    return ret_str

@app.route('/generateSSN')
def getSSN():
    ssn = SSNGenerator(r_conn).main()
    return jsonify({'SSN':ssn})


@app.route('/checkBadDebt/<string:ssn>')
def checkBadDebt(ssn: str):
    SSN_Status = CA(ssn=ssn).getCaponeStatus()
    if SSN_Status == 1:
        statusBadDebt='YES'
    else:
        statusBadDebt='NO'
    return jsonify({'statusBadDebt':statusBadDebt})

@app.route('/updateBadDebt/<string:ssn>')
def updateBadDebt(ssn: str):
    SSN_update = CA(ssn=ssn).updateCapone()
    return jsonify({'Status Message': SSN_update})

@app.route('/CreditRate_mock')
def _() -> str:
    return jsonify([{"queryId":"b2a91838-2d98-11eb-9851-0a8601fa0000","creditLimit":"5","collectionPlan":"HR","creditRate":"1500.00","fraudCheck":"OK","preventelCheck":"OK","reason":"Not in fraud","DiscountCheck":""}])

#@app.route('/getCommonSubscriber/<string:method>/<int:records>')
@app.route('/getSMSToken/<string:msisdn>')
def get_smstoken(msisdn: str):
    logging.info(env_token_host)
    token = SSH(env_token_host, msisdn).getToken()
    strip_token = token.strip("\n")
    return(jsonify(strip_token))

@app.route('/getSID/<string:msisdn>')
def get_sid(msisdn: str):
    logging.info(env_token_host)
    SID_LOG = SSH(env_token_host,msisdn).getSID()
    response = make_response(SID_LOG)
    response.headers["content-type"] = "text/plain"
    # strip_token = token.strip("\n")
    return response

@app.route('/updateRM/<string:msisdn>')
def update_rm1_res(msisdn: str):
    udp_rec = DatabaseCall(800,'022_Franciza_MNP_PREPAY_MOBILE_WITH_DISCFMC','msisdn',msisdn).updateRM1Res()
    return jsonify(udp_rec)

@app.route('/getSubId/<string:msisdn>')
def getSubId(msisdn:str):
    res = DatabaseCall(1,'dummy','dummy',msisdn).getSubId()
    return jsonify({'Result':res[0]})

@app.route('/getTestResults/<string:rls>', methods=['GET', 'POST'])
def add_rls(rls: str):
    a='test'
    if request.method == 'POST':
        content = request.json
        rj.jsonarrappend('test', Path('.'), content)
        print(content)
        return jsonify(rj.jsonget('test',no_escape=True))
    else:
        a= rj.jsonget('test',no_escape=False)
        logging.info(a)
        return jsonify(a)

@app.route('/getICCID/')
def geticcid():
    if os.environ['ENV'] == '20':
        sql = "select res from (select t.resource_value res from Rm1_Resource@CM20.NEW t where t.resource_status='ASSIGNED' and t.resource_value like '89%' and length(t.resource_value)>10 order by dbms_random.random) where rownum<=10"
    else:
        sql = "select res from (select t.resource_value res from Rm1_Resource@CM30 t where t.resource_status='ASSIGNED' and t.resource_value like '89%' and length(t.resource_value)>10 order by dbms_random.random) where rownum<=10"
    iccid_list = DatabaseCall(800,sql,'store',env_db_query).getICCID()
    records =  request.args.get('records', default = 1, type = int)
    rand_list = random.choices(iccid_list,k=records)
    d={}
    d['ICCID']=rand_list
    return (jsonify(d))


@app.route('/getPrereq/get_SIM/<string:env>')
def get_SIM(env:str):
    andrei_gh = database_Connection(host="10.230.167.129", port=1524, serviceName='EDWTST', user="andreigh", password='uat123')
    if 'CM20' in env.upper():
        query = "select res from (select t.resource_value res from Rm1_Resource@CM20.NEW t where t.resource_status='ASSIGNED' and t.resource_value like '89%' and length(t.resource_value)>10 order by dbms_random.random) where rownum<=1"
    else:
        query = "select res from (select t.resource_value res from Rm1_Resource@CM30.WORLD t where t.resource_status='ASSIGNED' and t.resource_value like '89%' and length(t.resource_value)>10 order by dbms_random.random) where rownum<=1"

    result = andrei_gh.exec_Select(query)
    andrei_gh.db_conn.close()
    return jsonify(result)


@app.route('/getPrepaid/')
def getprepaid():
    records =  request.args.get('records', default = 1, type = int)
    rand_list = random.choices(prepaid_list,k=records)
    d={}
    d['PreMSISDN']=rand_list
    return jsonify(d)

@app.route('/getCommonSubscriber/')
def add():
    username =  request.args.get('username', default = 'mihaela.filip', type = str)
    storeid =  request.args.get('storeid', default = 1043, type = int)
    records =  request.args.get('records', default = 5, type = int)
    method = request.args.getlist("method")
    print(method)
    if set(method).issubset(set(getAcceptedMethods('store'))) and records < 6:
        channel = 'store'
    elif set(method).issubset(set(getAcceptedMethods('franciza'))) and records < 6:
        channel = 'franciza'
    else:
        return jsonify(message= "No Records must be less than 6 && " +  "FRANCIZA: " + str(getAcceptedMethods('franciza'))+
        'STORE: '+ str(getAcceptedMethods('store'))),400
    rand_list = random.choices(subs_tuple,k=records)
    d={}
    for template in request.args.getlist("method"):
        req = DatabaseCall(10,template,channel,env_db_query).getReqTemp()
        returned = Subscribers(records,rand_list,req,username,storeid).getCommonSubscriber()
        subs_id=rand_list[0][1]
        if (returned[subs_id]) == 200:
            d[template]=returned
    logging.info('Exist objs')
    return jsonify(d)

@app.route('/executeQuery/<string:sql>')
def query(sql: str):
    req = DatabaseCall(10,sql,'dummy',env_db_query).getAnyQuery()
    # print(req)
    return ast.literal_eval(json.dumps(req))


@app.route('/dbActions/getQueryResult',methods=['GET'])
def getQueryResult():
    request_data = request.json
    logging.info('Request for query: '+ str(request_data['Query']))
    req = DatabaseCall(10,request_data['Query'],'dummy',env_db_query).getNonClobQuery()
    logging.info('Resp for query: '+ str(req))
    return {'Response':req}

@app.route('/dbActions/getCCQuery',methods=['GET'])
def getCCQueryResult():
    req = DatabaseCall(10,'select * from TABLE_COMMON_CUSTOMER','dummy',env_db_query).getNonClobQuery()
    logging.info('Resp for query: '+ str(req))
    return {'data':req}

@app.route('/getCommonSubscriber/<int:msisdn>')
#here we have a filter to be added after uri ?chnl=franciza , will switch the default
def msisdn(msisdn: int):
    channel =  request.args.get('chnl', default = 'store', type = str)
    username =  request.args.get('username', default = 'mihaela.filip', type = str)
    storeid =  request.args.get('storeid', default = 1043, type = int)
    records =  request.args.get('records', default = 5, type = int)
    dic = {}
    sub_list = []
    # abc = [(1234,1111),(222,333)]
    print((subs_tuple))
    for subs_msisdn in subs_tuple:
        if subs_msisdn[1] == str(msisdn):
            print(subs_msisdn[0])
            sub_list.append(subs_msisdn)

    print(sub_list)
    for f in getAcceptedMethods(channel):
        req = DatabaseCall(1,f,channel,env_db_query).getReqTemp()
        returned = Subscribers(1,sub_list,req,username,storeid).getCommonSubscriber()
        dic[f]=returned
    return jsonify(dic)


@app.route('/getCommonSubscriberSet/<int:no_of_subscribers>')
#here we have a filter to be added after uri ?chnl=franciza , will switch the default
def getfullset(no_of_subscribers: int):
    channel =  request.args.get('chnl', default = 'store', type = str)
    username =  request.args.get('username', default = 'mihaela.filip', type = str)
    storeid =  request.args.get('storeid', default = 1043, type = int)
    records =  request.args.get('records', default = 5, type = int)
    #subscriberi = DatabaseCall(100,'dummy','dummy',env_db_query).getSubsList()
    dic = {}
    rand_list = random.choices(subs_tuple,k=no_of_subscribers)
    for template in getAcceptedMethods(channel):
        req = DatabaseCall(1,template,channel,env_db_query).getReqTemp()
        returned = Subscribers(no_of_subscribers,rand_list,req,username,storeid).getCommonSubscriber()
        dic[template]=returned
    return jsonify(dic)


@app.route('/getData/pegaOffers', methods=['GET'])
def pegaUtils():

    req_data = request.json
    print(req_data)
    # req_data = ast.literal_eval(req_data)
    respone = getPegaOffers.main(req_data)
    if respone == None:
        respone = {}
    return respone


@app.route('/executionSummary/appendExcelData', methods=['GET'])
def setExcelValuesForExecution():
    req_data = request.json
    logging.info(req_data)
    writeToExcel = executionDetails.ExecutionDetails(req_data)
    writeToExcel.main()
    return jsonify("async queued")


@app.route('/insertCR', methods=['GET'])
def insertCR():
    if os.environ['ENV'] == '30':
        bdp_env = 'BDP2'
    else:
        bdp_env = 'BDP1'

    req_data = request.json
    insertSSN = DatabaseCall(no_rec=bdp_env,s_method=req_data['SSN'],flow_type=req_data['CR'],env='ANDREIGH_LOG').insertSSN()
    if type(insertSSN) == int:
        rows_inserted = str(insertSSN)
        status_code = "0"
        status_message = "Success"

    else:
        rows_inserted = "0"
        status_code = insertSSN.code
        status_message = insertSSN.message
    rsp_json = {'Rows inserted': rows_inserted, 'StatusCode': status_code, 'StatusMessage': status_message}
    return jsonify(rsp_json)


@app.route('/getPrereq/LoginCredentials', methods=['GET'])
def loginCredentials():
    input_Data = request.json
    getPrereq = getPrerequisites(inputDictionary=input_Data)
    return jsonify(getPrereq.get_User_and_Pass())

@app.route('/getPrereq/searchResource', methods=['GET'])
def searchResource():
    input_Data = request.json
    getPrereq = getPrerequisites(inputDictionary=input_Data)
    return jsonify(getPrereq.get_Search_Resources())

@app.route('/getPrereq/get_Customer/<string:env>', methods=['GET'])
def getCustomer(env: str):
    # a =    [
    #     {
    #     "customerType": "Common",
    #     "master": "OMS",
    #     "status": "all_Cancelled",
    #     # 'get_full': 'true'
    #     },
    #     {
    #     "customerType": "notcommon",
    #     "master": "OMS",
    #     "status": "all_Cancelled",
    #     'get_full': 'true'
    #     },
    #     {
    #     "customerType": "notcommon",
    #     "master": "OMS",
    #     "status": "Active",
    #     # 'get_full': 'true'
    #     },
    #     {
    #     "customerType": "Common",
    #     "master": "OMS",
    #     "status": "Active",
    #     # 'get_full': 'true'
    #     }
    #     ]
    input_Data = request.json
    setCustomer = get_Customer.common_NotCommon_Customer(input_Data, r_conn, env)
    respose = setCustomer.main()
    return jsonify(respose)

@app.route('/getPrereq/get_Username', methods=['GET'])
def getUsername():
    input_Data = request.json
    if 'env' in input_Data.keys() and 'channel' in input_Data.keys():
        response = get_username.get_username(input_Data)
    else:
        response = {"Username": "NotFound", "Error": "env and channel missing on input request"}
    return jsonify(response)

@app.route('/getPrereq/get_device_id', methods=['GET'])
def getDevice_Id():
    input_Data = request.json
    my_dev_id = device_id(input_Data['env'], input_Data['device_name'])
    result = my_dev_id.get_devId()

    response = {"Result": result}

    return jsonify(response)

@app.route('/update_Prereq/clean_sim', methods=['GET'])
def clean_sim():
    input_Data = request.json
    try:
        result = clean_Sim.clean_sim(input_Data['env'], input_Data['sim'])
    except Exception:
        result = 'Fail'
    response = {"Result": result}

    return jsonify(response)

@app.route('/update_Prereq/clean_msisdn', methods=['GET'])
def clean_msisdn():
    input_Data = request.json
    try:
        result = clean_msisdn.do_clean_msisdn(input_Data['env'], input_Data['msisdn'])
    except Exception:
        result = 'Fail'
    response = {"Result": result}

    return jsonify(response)

@app.route('/get_Prereq/get_IMEI', methods=['GET'])
def get_IMEI():
    input_Data = request.json
    try:
        result = get_device_IMEI.get_IMEI(input_Data['env'], input_Data['deviceId'])
    except Exception:
        result = 'Fail'
    response = {"Result": result}

    return jsonify(response)

@app.route('/get_Prereq/getLog', methods=['GET'])
def get_log():
    input_Data = request.json
    try:
        result = getlogfile.getToken('dex2')
    except Exception:
        result = 'Fail'
    response = {"Result": result}

    return jsonify(response)

@app.route('/getPrereq/get_migratedCust', methods=['GET'])
def get_migratedCust():
    # request_details = \
    #     [{
    #         "customerType": "P",  # all
    #         "subscriberType": "FI",  # FI/TV/all - FI <=> only FI
    #         "socType": "P",  # P/U
    #         "env": "CM30"
    #     }]
    input_Data = request.json
    try:
        response = query_Builder(input_Data=input_Data).main()
    except Exception as e:
        response = e

    return jsonify(response)

# get_migratedCust with params
api.add_resource(get_migratedCustParams, '/getPrereq/get_migrCustParams')


if __name__ == '__main__':
    app.run(host='0.0.0.0')


