import sys, time, threading

def your_function_name() :
    time.sleep(4)

def loadingAnimation(process) :
    while process.is_alive() :
        chars = "/—\|"
        for char in chars:
            sys.stdout.write('\r'+'loading '+char)
            time.sleep(.1)
            sys.stdout.flush()

loading_process = threading.Thread(target=your_function_name)
loading_process.start()
loadingAnimation(loading_process)
loading_process.join()