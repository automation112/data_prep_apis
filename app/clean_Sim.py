from connect_to_db import database_Connection


def clean_sim(env, sim):
    andrei_gh = database_Connection(host="10.230.167.129", port=1524, serviceName='EDWTST', user="andreigh", password='uat123')
    if 'CM20' in env.upper():

        query_1 = f"""update rm1_resource@Cm20.new
set RESOURCE_STATUS = 'ASSIGNED', LAST_ACTIVITY_NAME = 'RELEASE'
where RESOURCE_TYPE_ID in(29, 30)
and RESOURCE_PARENT_VALUE = {sim}
and RESOURCE_STATUS like 'RESERVED'"""

        query_2 = f"""update rm1_package@Cm20.new
set PACKAGE_STATUS = 'ASSIGNED', LAST_ACTIVITY_NAME = 'RELEASE'
where
PACKAGE_VALUE = {sim}
and PACKAGE_STATUS like 'TMP_RESERVED'"""

    else:
        query_1 = f"""update rm1_resource@CM30.WORLD
        set RESOURCE_STATUS = 'ASSIGNED', LAST_ACTIVITY_NAME = 'RELEASE'
        where RESOURCE_TYPE_ID in(29, 30)
        and RESOURCE_PARENT_VALUE = {sim}
        and RESOURCE_STATUS like 'RESERVED'"""

        query_2 = f"""update rm1_package@CM30.WORLD
        set PACKAGE_STATUS = 'ASSIGNED', LAST_ACTIVITY_NAME = 'RELEASE'
        where
        PACKAGE_VALUE = {sim}
        and PACKAGE_STATUS like 'TMP_RESERVED'"""

    result = 'Succesfuly'

    rslt = andrei_gh.exec_update(query_1)
    rslt += andrei_gh.exec_update(query_2)
    andrei_gh.db_conn.close()
    return (result + f' updated {rslt} rows')

if __name__ == '__main__':
    print(clean_sim('CM20', 8940011006183029217))