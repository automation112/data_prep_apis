def get_username(input_json):
  # {"channel": "[TLS - Active - Common]", "env": "dexuat1"}
  usr = {
      "dexuat1": {
          "TLS": 'cm.test1',
          "Dealer": 'diana.busca1',
          "Store": 'mihaela.filip',
          "[NON FRUIT]": 'florin.dedu'
      },
      "dexuat2": {
          "TLS": 'cm.test1',
          "Dealer": 'mihaela.filip1',
          "Store": 'mihaela.filip1',
          "[NON FRUIT]": 'florin.dedu'
      }
  }
  try:
    return {"Username": usr[input_json["env"]][input_json["channel"]]}
  except KeyError:
    return {"Error": f"For env = {input_json['env']} and channel = {input_json['channel']} no users found",
            "Available_data": usr}