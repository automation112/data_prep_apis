

class getPrerequisites:

    def __init__(self, inputDictionary):
        self.inputDictionary = inputDictionary

    def get_User_and_Pass(self):
        dynamicSelector = {'Dealer + FRUT + Agent': self.Dealer_FRUT_Agent(),
                           "TLS + FRUT + Agent": self.TLS_FRUT_Agent()}
        return dynamicSelector[self.getChild(self.inputDictionary, 'agentType')]

    def get_Search_Resources(self):
        dynamicSelector = {'msisdn_allowed': self.MSISDN_allowed(),}
        return dynamicSelector[(self.getChild(self.inputDictionary, 'searchType') + '_' +self.getChild(self.inputDictionary, 'subscriberValidation')).lower()]

    def MSISDN_allowed(self):
        if self.getChild(self.inputDictionary, 'searchType') == "MSISDN" and self.getChild(self.inputDictionary, 'subscriberValidation') == "allowed":
            return {'searchValue': '724343010'}

    def Dealer_FRUT_Agent(self):
        if self.getChild(self.inputDictionary, 'agentType') == "Dealer + FRUT + Agent":
            return {'username': 'diana.busca1', 'password': "Arsis"}

    def TLS_FRUT_Agent(self):
        if self.getChild(self.inputDictionary, 'agentType') == "TLS + FRUT + Agent":
            return {'username': 'cm.test1', 'password': "123"}
    #push
    def getChild(self, inputDictionary, key):
        if inputDictionary is None:
            inputDictionary = {'Not Instantiated': 'TBD'}
        try:
            return inputDictionary[key]
        except (KeyError, ValueError) as e:
            return None

if __name__ == '__main__':
    getPrerequisites = getPrerequisites({'agentType': 'Retail (Franciza & Store)', 'dealerCode': 'One_dealerCode'})
    print(getPrerequisites.get_Search_Resources())