import os
import sys
import paramiko
from os.path import expanduser
import json

class SSH():
    def __init__(self,host_name,msisdn):
        self.host_name=host_name
        self.msisdn=msisdn


    def getToken(self):
        home = expanduser("~")
        command = """/home/razvan.dumitrascuta/getToken.sh """ + str(self.msisdn) + ' TOKEN'
        conf = paramiko.SSHConfig()
        #print(os.path.expanduser(home+'/.ssh/config'))
        conf.parse(open(home+'/.ssh/config'))
        host = conf.lookup(self.host_name)
        client = paramiko.SSHClient()
        client.load_system_host_keys()
        #client.set_missing_host_key_policy('AutoAddPolicy')
    
        client.connect(
            host['hostname'], username=host['user'],
            key_filename=host['identityfile'], 
            banner_timeout=30,auth_timeout=10,
            sock=paramiko.ProxyCommand(host.get('proxycommand'))
        )
        stdin, stdout, stderr = client.exec_command(command)
        stdin.write('Yes\n')
        paramiko.util.log_to_file("paramiko.log")
        r_stdout = stdout.read().decode("utf-8").rstrip()
        client.close()
        return r_stdout
    
    def getSID(self):
        home = expanduser("~")
        command = """/home/razvan.dumitrascuta/getToken.sh """ + str(self.msisdn) + ' SID'
        conf = paramiko.SSHConfig()
        #print(os.path.expanduser(home+'/.ssh/config'))
        conf.parse(open(home+'/.ssh/config'))
        host = conf.lookup(self.host_name)
        client = paramiko.SSHClient()
        client.load_system_host_keys()
        #client.set_missing_host_key_policy('AutoAddPolicy')
    
        client.connect(
            host['hostname'], username=host['user'],
            key_filename=host['identityfile'], 
            banner_timeout=30,auth_timeout=10,
            sock=paramiko.ProxyCommand(host.get('proxycommand'))
        )
        stdin, stdout, stderr = client.exec_command(command)
        stdin.write('Yes\n')
        paramiko.util.log_to_file("paramiko.log")
        r_stdout = stdout.read().decode("utf-8").rstrip()
        client.close()
        return r_stdout

    
 
if __name__ == '__main__':
    print(SSH('uat-dex-app5','11112222').getToken())
