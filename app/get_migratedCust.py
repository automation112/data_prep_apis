import cx_Oracle
import constants as db_details
from flask_restful import reqparse, Resource

# # connection by Decorators
# def db_connector(func):
#     def with_connection_(*args, **kwargs):
#         conn_str = os.environ["CONN"]
#         cnn = cx_Oracle.connect(conn_str)
#         try:
#             rv = func(cnn, *args, **kwargs)
#         except Exception:
#             cnn.rollback()
#             logging.error("Database connection error")
#             raise
#         else:
#             cnn.commit()
#         finally:
#             cnn.close()
#         return rv
#     return with_connection_

# class for get_migratedCust with body
class query_Builder:

    query = """
                SELECT 
                    --S.CUSTOMER_ID, C.CUSTOMER_TYPE, C.EXTERNAL_ID, S.SUBSCRIBER_NO, S.PRIM_RESOURCE_VAL, 
                    --S.SUBSCRIBER_TYPE,
                    --SUBSTR(anl.search_elem2,1,instr(anl.search_elem2,' ')-1) CUI, 
                    ANL.SEARCH_ELEM2 CNP
                    --count(ANL.SEARCH_ELEM2)
                    -- CO.SOC_TYPE, CO.SOC_NAME
                FROM 
                    SUBSCRIBER        S,
                    ADDRESS_NAME_LINK ANL,
                    CUSTOMER          C,
                    SERVICE_AGREEMENT SA,
                    CSM_OFFER         CO
                WHERE 
                    CO.SOC_CD = SA.SOC
                    AND S.SUBSCRIBER_NO = SA.AGREEMENT_NO
                    AND ANL.ENTITY_ID = S.CUSTOMER_ID
                    AND ANL.LINK_TYPE = 'C'
                    AND C.CUSTOMER_ID = S.CUSTOMER_ID
                    -- and ANL.SEARCH_ELEM2 like '1390513293121'
                    AND C.CUSTOMER_TYPE = '_customerType_'
                    AND ANL.EXPIRATION_DATE IS NULL
                    AND SA.EXPIRATION_DATE IS NULL
                    AND c.customer_id IN ( SELECT * from trtr_cust_id )
                    and soc_type = '_socType_'
                    and subscriber_type in ('_subscriberType_')
                    --having count(ANL.SEARCH_ELEM2)=2
                    and ROWNUM <= 50
                    having length(ANL.SEARCH_ELEM2) =13
                    group by ANL.SEARCH_ELEM2,  S.SUBSCRIBER_TYPE
                    ORDER BY 1
    """

    def __init__(self, input_Data):
        self.input_Data = input_Data

    def outputSorting(self, results):

        import random
        randomCNP_Nr = random.randint(0, len(results) - 1)
        # print(results[randomCNP_Nr])
        return results[randomCNP_Nr]

    def exec_Select(self, con):

        results = []

        if self.input_Data[0]['subscriberType'] == 'all':
            self.input_Data[0]['subscriberType'] = "FI','TV"
        custom_query = str(self.query).replace('_subscriberType_', str(self.input_Data[0]['subscriberType'])).replace('_socType_', str(self.input_Data[0]['socType'])).replace('_customerType_', str(self.input_Data[0]['customerType']))
        # print(custom_query)
        new_cursor = con.cursor()
        new_cursor.execute(custom_query)

        columns = [column[0] for column in new_cursor.description]

        for row in new_cursor.fetchall():
            results.append(dict(zip(columns, row)))

        # print(self.outputSorting(results))
        return self.outputSorting(results)

    def main(self):

        with oracle_connection(connection_string=db_details.get_ConnDetails(self.input_Data[0]['env'])) as con:
            return self.exec_Select(con)


# connection by Context Managers
class oracle_connection(object):
    """oracle db connection"""

    def __init__(self, connection_string):
        self.connection_string = connection_string
        self.connector = None
        self.host = connection_string['host']
        self.port = connection_string['port']
        self.serviceName = connection_string['serviceName']
        self.user = connection_string['user']
        self.password = connection_string['password']

    def __enter__(self):
        dsn_tns = cx_Oracle.makedsn(self.host, self.port, service_name=self.serviceName)
        self.connector = cx_Oracle.connect(user=self.user, password=self.password, dsn=dsn_tns)
        print('connected to db version', self.connector.version)
        return self.connector

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_tb is None:
            self.connector.commit()
        else:
            self.connector.rollback()
        self.connector.close()
        print('exited db')


# class for get_migratedCust with Params
class get_migratedCustParams(Resource):

    def __init__(self):

        self.parser = reqparse.RequestParser()
        self.parser.add_argument('customerType', type=str, required=True, help='Enter customerType: P or C')
        self.parser.add_argument('subscriberType', type=str, required=True, help='Enter subscriberType: TV or FI or all')
        self.parser.add_argument('socType', type=str, required=True, help='Enter socType: P or U')
        self.parser.add_argument('env', type=str, required=True, help='Enter env: CM10')

        self.__customerType = self.parser.parse_args().get('customerType', None)
        self.__subscriberType = self.parser.parse_args().get('subscriberType', None)
        self.__socType = self.parser.parse_args().get('socType', None)
        self.__env = self.parser.parse_args().get('env', None)

    def get(self):

        input_data = []

        try:
            input_data.append(self.parser.parse_args())
            response = query_Builder(input_Data=input_data).main()
        except Exception as e:
            response = e

        return response

        # if self.__customerType == 'P':
        #     print('ok')
        #     return self.parser.parse_args()
        # else:
        #     return {'hmm': 'ok'}

if __name__ == '__main__':

    input_Data_Example = [{
        "customerType": "P",  # all
        "subscriberType": "TV",  # FI/TV/all - FI <=> only FI
        "socType": "P",  # P/U
        "env": "CM30"
    }]
    # print('ok')
    # print(input_Data[0]['env'])
    # print(object(input_Data[0]['env']))
    # print(db_details.get_ConnDetails(input_Data_Example[0]['env']))
    # cust = query_Builder(input_Data=input_Data_Example)
    # print(cust.main())