from selenium import webdriver
import os
import time, bcolors, sys


time.sleep(1)
caps = webdriver.DesiredCapabilities.CHROME.copy()
caps['goog:loggingPrefs'] = {'performance': 'ALL'}
options = webdriver.ChromeOptions()
# options.add_argument(("--proxy-server={0}".format(proxy.proxy)))
options.add_argument("--start-maximized")
options.add_argument('ignore-certificate-errors')
options.add_argument("--incognito")
# options.add_argument('headless')
# driver = ChromeDriver(options)
caps['acceptInsecureCerts'] = True

SRCPath = os.path.dirname(os.path.abspath(__file__)).replace(r'\SRC\test_Definitions', '')
projectDir = '{}/Resources/Drivers'.format(SRCPath)
executable_path = os.path.join(projectDir, "chromedriver.exe")
print(executable_path)
# browser = webdriver.Firefox(executable_path=executable_path,options=options)
browser = webdriver.Chrome(executable_path=executable_path, options=options, desired_capabilities=caps)
browser.get('https://isj.educv.ro/cnp/')
browser.find_element_by_xpath("//input[@id='datepicker']").click()
browser.find_element_by_xpath("//input[@id='datepicker']").send_keys("08.01.1992")
browser.implicitly_wait(15)


for i in range(3000):
    browser.find_element_by_xpath("//input[@id='genButton']").click()
    print(browser.find_element_by_xpath("//span[@id='cnp']").text)
