import datetime
import logging

class SSNGenerator:
    def __init__(self,redis_conn):
        self.redis_conn = redis_conn                                   
        if self.redis_conn.get('CONTOR') is None:
            self.redis_conn.set('CONTOR','100')
        if self.redis_conn.get('DAY') is None:
            self.redis_conn.set('DAY','01')
        if self.redis_conn.get('MONTH') is None:
            self.redis_conn.set('MONTH','01')
        if self.redis_conn.get('YEAR') is None:
            self.redis_conn.set('YEAR',1950)

    def setNextDay(self,day, month, year):
        date = datetime.date(year, month, day)
        date += datetime.timedelta(days=1)
        return date

    def getCurrentDate(self):
        an = self.redis_conn.get('YEAR').decode("utf-8") 
        luna = self.redis_conn.get('MONTH').decode("utf-8") 
        zi = self.redis_conn.get('DAY').decode("utf-8") 
        return {'year':an,'month':luna, 'day':zi}

    def generateSSN(self):
        sex = '1'
        county = '13'
        county1 = county[0:1]
        county2 = county[1:2]
        currentDate = self.getCurrentDate()
        logging.info(currentDate)
        SSNyear1 = currentDate['year'][2:3]
        SSNyear2 = currentDate['year'][3:4]
        SSNmonth1 = currentDate['month'][0:1]
        SSNmonth2 = currentDate['month'][1:2]
        SSNday1 = currentDate['day'][0:1]
        SSNday2 = currentDate['day'][1:2]
        contor = self.redis_conn.get('CONTOR').decode("utf-8")
        orderNumber1 = contor[0:1]
        orderNumber2 = contor[1:2]
        orderNumber3 = contor[2:3]
        logging.info(sex,SSNyear1,SSNyear2,SSNmonth1,SSNmonth2,SSNday1,SSNday2,county1,county2,orderNumber1,orderNumber2,orderNumber3)
        cksum = int(sex) * 2 + int(SSNyear1) * 7 + int(SSNyear2) * 9 + int(SSNmonth1) * 1 + int(SSNmonth2) * 4 + int(
            SSNday1) * 6 + int(SSNday2) * 3 + int(county1) * 5 + int(county2) * 8 + int(orderNumber1) * 2 + int(
            orderNumber2) * 7 + int(orderNumber3) * 9
        controlCalculat = (int(cksum) % 11)
        if controlCalculat < 10:
            controlCalculat = controlCalculat
        elif controlCalculat == 10:
            controlCalculat = 1
        cnp = sex + SSNyear1 + SSNyear2 + SSNmonth1 + SSNmonth2 + SSNday1 + SSNday2 + county1 + county2 + orderNumber1 + orderNumber2 + orderNumber3 + str(
            controlCalculat)
        return cnp

    def main(self):
        if int(self.redis_conn.get('CONTOR')) == 999:
            redis_date = self.getCurrentDate()
            self.setNextDay(day=redis_date['DAY'],month=redis_date['MONTH'],year=redis_date['YEAR'])
            self.redis_conn.set('CONTOR',100)
        SSN = self.generateSSN()
        self.redis_conn.incr('CONTOR')
        return SSN