from connect_to_db import database_Connection


class device_id:

    def __init__(self, env, device_name):
        self.env = env
        self.device_name = device_name
        self.database_info = self.get_conn_data()

    def get_devId(self):
        my_db_conn = database_Connection(host=self.database_info['HOST'], port=self.database_info['PORT'], serviceName=self.database_info['SERVICE_NAME'], user=self.database_info['USER'], password=self.database_info['PASS'])
        query = f"select ITEM_CODE from vw_epc_device_promotion where commer_name = '{self.device_name}'"
        resutl = my_db_conn.exec_Select(query)
        my_db_conn.db_conn.close()
        return resutl

    def get_conn_data(self):
        env_conn = {

            'PEGA_UAT1': {
                'HOST': "'uat-pega1.cakse7fwad5c.eu-central-1.rds.amazonaws.com'",
                'PORT': '1521',
                "SERVICE_NAME": 'PEGA1UAT',
                'USER': 'APPLICATION_AREA',
                'PASS': 'APPLICATION_AREA_12345'
            },

            'PEGA_UAT2': {'HOST': 'uat-pega2.cakse7fwad5c.eu-central-1.rds.amazonaws.com',
                         'PORT' : '1521',
                         "SERVICE_NAME" : 'PEGA2UAT',
                         'USER': 'APPLICATION_AREA',
                         'PASS': 'APPLICATION_AREA_12345'
            },

            'PEGA_UAT3': {'HOST': 'uat-pega3.cakse7fwad5c.eu-central-1.rds.amazonaws.com',
                                 'PORT': '1521',
                                 'SERVICE_NAME': 'PEGA3UAT',
                                 'USER': 'APPLICATION_AREA',
                                 'PASS': 'APPLICATION_AREA_12345'
                                 }}

        return env_conn[self.env]

if __name__=='__main__':
    my_dev_id = device_id('PEGA_UAT3', 'Samsung Galaxy S6 32GB Negru')
    print(my_dev_id.get_devId())
