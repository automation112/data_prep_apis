from connect_to_db import database_Connection
import random


def get_IMEI(env, deviceId):
    '''SIM1.WORLD=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=savage.connex.ro)(PORT=1521))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=SIM1)))'''
    '''SIM2.WORLD=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=savage.connex.ro)(PORT=1521))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=SIM2)))'''

    conn_data = {'SIM1':{'HOST': 'savage.connex.ro',
            'port': 1521,
            'sid': 'SIM1',
            'user': 'oraskillnet',
            'pass': 'qwerty_9876'},

                 'SIM2': {'HOST': 'savage.connex.ro',
            'port': 1521,
            'sid': 'SIM2',
            'user': 'oraskillnet',
            'pass': 'test123'}}

    db_connection = database_Connection(conn_data[env]['HOST'], port=conn_data[env]['port'], serviceName=conn_data[env]['sid'], user=conn_data[env]['user'], password=conn_data[env]['pass'])
    result = db_connection.exec_Select(
        f"select uin from simuser.uin_detail t where t.item_id = '{deviceId}' and t.store_id = '10043' and t.status =0")

    db_connection.db_conn.close()
    try:
        return result[random.randint(0, len(result) - 1)]
    except Exception:
        return 'FAIL'

print(get_IMEI('SIM1', 'TAP732GBN'))