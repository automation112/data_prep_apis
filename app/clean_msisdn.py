from connect_to_db import database_Connection


def do_clean_msisdn(env, msisdn):
    andrei_gh = database_Connection(host="10.230.167.129", port=1524, serviceName='EDWTST', user="andreigh", password='uat123')
    if 'CM20' in env.upper():
        db_link = 'Cm20'
    else:
        db_link = 'Cm30'


    query = f"""update rm1_resource@{db_link}.new
        set RESOURCE_STATUS = 'ASSIGNED', LAST_ACTIVITY_NAME = 'RELEASE'
        where RESOURCE_TYPE_ID = 8
        and RESOURCE_VALUE = {msisdn}
        and RESOURCE_STATUS like 'TMP_RESERVED'"""

    result = 'Succesfuly'
    count = andrei_gh.exec_update(query)
    andrei_gh.db_conn.close()
    return (result + f" updated {count} rows")