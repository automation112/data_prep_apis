import cx_Oracle
import requests
from copy import deepcopy
import json
import generateSSN

class database_Connection:

    db_conn = None

    def __init__(self, host, port, serviceName, user, password):
        self.host = host
        self.port = port
        self.serviceName = serviceName
        self.user = user
        self.password = password
        self.connect_to_db()

    def connect_to_db(self):
        dsn_tns = cx_Oracle.makedsn(self.host, self.port,
                                    service_name=self.serviceName)  # if needed, place an 'r' before any parameter in order to address special characters such as '\'.
        self.db_conn = cx_Oracle.connect(user=self.user, password=self.password,
                                 dsn=dsn_tns)  # if needed, place an 'r' before any parameter in order to address special characters such as '\'. For example, if your user name contains '\', you'll need to place 'r' before the user name: user=r'User Name'


class common_NotCommon_Customer:

    cache = {}
    oms_select = """SELECT DISTINCT BUS.x_identity_number
                --part_status
                --select tc.*
                FROM   table_customer tc,
                       table_site_part tsp,
                       table_bus_org bus,
                       mtm_bus_org95_customer1 mtm
                WHERE  tc.objid = TSP.site_part2customer
                       AND bus.objid = MTM.bus_org2customer
                       AND tc.objid = MTM.customer2bus_org
                       AND tc.type = 'Residential Customer'
                       AND TSP.part_status = '_placeholder_'
                       AND TC.x_is_external = '0'
                       AND tsp.instance_name = 'Mobile'
                       and upper(X_IDENTITY_NUMBER) = lower(X_IDENTITY_NUMBER)
                    --added condition for CNP length
                       having length(bus.X_IDENTITY_NUMBER) =13
                       group by X_IDENTITY_NUMBER"""

    vos_select = """select DISTINCT BUS.X_IDENTITY_NUMBER
                    from    table_customer tc,
                            table_site_part tsp,
                            table_bus_org bus,
                            MTM_BUS_ORG95_CUSTOMER1 mtm
                    where   tc.objid=TSP.SITE_PART2CUSTOMER
                            and bus.objid=MTM.BUS_ORG2CUSTOMER
                            and tc.objid=MTM.CUSTOMER2BUS_ORG
                            and tc.type='Residential Customer'
                            and TSP.PART_STATUS='_placeholder_'
                            and TC.X_IS_EXTERNAL='1'
                            and tsp.instance_name='CBU Subscription'
                            and upper(X_IDENTITY_NUMBER) = lower(X_IDENTITY_NUMBER)
                        --added condition for CNP length
                            having length(bus.X_IDENTITY_NUMBER) =13
                            group by X_IDENTITY_NUMBER"""


    def __init__(self, input_List, redis_conn, env):
        self.input_List = input_List
        self.cache['count'] = -1
        self.cache['customer_id'] = []
        self.cache['common'] = []
        self.cache['not_common'] = []
        self.redis_conn = redis_conn
        self.env = env

        if env == 'CM30':
            self.crm_Conn = database_Connection(host="alesi", port='1523', serviceName="VFRCRM30", user="SA", password="SA")
            self.ods_SELONLY = database_Connection(host="db-ods-uat2.connex.ro", port='1521', serviceName="ODSUAT2", user="selonly", password="readonly")
            self.ods_IODS = database_Connection(host="db-ods-uat2.connex.ro", port='1521', serviceName="ODSUAT2", user="IODS", password="passw0rd")
        elif env == 'CM20':
            self.crm_Conn = database_Connection(host="alesi", port='1523', serviceName="VFRCRM20", user="SA",
                                                  password="SA")
            self.ods_SELONLY = database_Connection(host="roods-t-scan.connex.ro", port='1521', serviceName="ODSUAT1",
                                                    user="selonly", password="readonly")
            self.ods_IODS = database_Connection(host="db-ods-uat2.connex.ro", port='1521', serviceName="ODSUAT2",
                                                 user="IODS", password="passw0rd")

        self.result = {}

    def exec_Select(self, conn, query):
        results = []
        cursor = conn.cursor()
        cursor.execute(query)  # use triple quotes if you want to spread your query across multiple lines
        columns = [column[0] for column in cursor.description]

        for row in cursor.fetchall():
            results.append(dict(zip(columns, row)))

        return results

    def exec_Insert(self, conn, query):
        cursor = conn.cursor()
        cursor.execute(query)  # use triple quotes if you want to spread your query across multiple lines
        print(cursor.rowcount())

    def load_data(self, input_dictionary):

        query = self.get_query(input_dictionary)
        self.cache['customers_List'] = self.exec_Select(self.crm_Conn.db_conn, query=query.replace("_placeholder_", input_dictionary['status'].replace('all_', '')))
        self.cache['temp_cancelled'] = self.exec_Select(self.crm_Conn.db_conn, query=query.replace("_placeholder_", 'Cancelled'))
        # print('#################', self.cache['customers_List'] )
        for i in self.cache['temp_cancelled']:
            try:
                self.cache['customers_List_cancelled'].append(int(i['X_IDENTITY_NUMBER']))
            except KeyError:
                self.cache['customers_List_cancelled'] = []
                self.cache['customers_List_cancelled'].append(int(i['X_IDENTITY_NUMBER']))

    def get_query(self, input_dictionary):
        if input_dictionary['master'].lower() == 'oms':
            return self.oms_select
        else:
            return self.vos_select

    def identify_RequiredCustomer(self, input_dictionary):
        count = 0
        if input_dictionary['customerType'].lower() == 'newcustomer':
            result_key = f"{input_dictionary['customerType']}_{input_dictionary['master']}_{input_dictionary['status']}"
            randomCNP = generateSSN.SSNGenerator(redis_conn=self.redis_conn)
            self.result[result_key].append({f'CNP': randomCNP.main()})

        else:
            for customer_dictionary in self.cache['customers_List']:
                if self.get_CustomerType(customer_dictionary['X_IDENTITY_NUMBER']) == input_dictionary['customerType'].lower():
                    if input_dictionary['status'].lower() == 'active':
                        if int(customer_dictionary['X_IDENTITY_NUMBER']) not in self.cache['customers_List_cancelled']:
                            result_key = f"{input_dictionary['customerType']}_{input_dictionary['master']}_{input_dictionary['status']}"
                            self.result = deepcopy(self.result)
                            self.result[result_key].append({f'CNP': customer_dictionary["X_IDENTITY_NUMBER"]})
                            count += 1
                            if count > 1:
                                break
                            # if input_dictionary['get_full'] != 'true':
                            #     break

                    elif input_dictionary['status'].lower() == 'cancelled':

                        result_key = f"{input_dictionary['customerType']}_{input_dictionary['master']}_{input_dictionary['status']}"
                        self.result = deepcopy(self.result)
                        self.result[result_key].append({f'CNP': customer_dictionary["X_IDENTITY_NUMBER"]})
                        # if input_dictionary['get_full'] != 'true':
                        #     break
                        count += 1
                        if count > 1:
                            break

                    elif input_dictionary['status'].lower() == 'all_cancelled':
                        q_result = self.is_all_cancelled(cnp=int(customer_dictionary['X_IDENTITY_NUMBER']), input_dictionary=input_dictionary)
                        # print(q_result)
                        if len(q_result) == 1 and q_result[0]['PART_STATUS'].lower() == 'cancelled':
                            result_key = f"{input_dictionary['customerType']}_{input_dictionary['master']}_{input_dictionary['status']}"
                            self.result = deepcopy(self.result)
                            self.result[result_key].append({f'CNP': customer_dictionary["X_IDENTITY_NUMBER"]})
                            # if input_dictionary['get_full'] != 'true':
                            #     break
                            count += 1
                            if count > 1:
                                break



    def get_CustomerType(self, customerId):
        isCommon = self.exec_Select(conn=self.ods_SELONLY.db_conn, query=f"select 1 from ETL_OP_BUFFER.NP_CUSTOMER a where a.customer_role = 'Active' and a.date_to = TO_DATE('99991231', 'YYYYMMDD') and a.ssn_cui in ('{customerId}')")

        if isCommon:
            return 'common'
        else:
            return 'notcommon'


    def get_customer(self, cust_list):
        self.cache['count'] += 1
        self.cache['customer_id'].append(list(cust_list[self.cache["count"]].values())[0])

    def is_all_cancelled(self, cnp, input_dictionary):
        if input_dictionary['master'].lower() == 'vos':
            query = f"""select X_IDENTITY_NUMBER, count(BUS.X_IDENTITY_NUMBER), PART_STATUS
                        from
                        table_customer tc,
                        table_site_part tsp,
                        table_bus_org bus,
                        MTM_BUS_ORG95_CUSTOMER1 mtm
                        where
                        tc.objid=TSP.SITE_PART2CUSTOMER
                        and bus.objid=MTM.BUS_ORG2CUSTOMER
                        and tc.objid=MTM.CUSTOMER2BUS_ORG
                        and tc.type='Residential Customer'
                        --and TSP.PART_STATUS='Active'
                        and TC.X_IS_EXTERNAL='1'
                        and X_IDENTITY_NUMBER = '{cnp}'
                        and tsp.instance_name='CBU Subscription'
                        group by X_IDENTITY_NUMBER, PART_STATUS"""
        else:
            query = f"""select X_IDENTITY_NUMBER, count(BUS.X_IDENTITY_NUMBER), PART_STATUS
                        from
                        table_customer tc,
                        table_site_part tsp,
                        table_bus_org bus,
                        MTM_BUS_ORG95_CUSTOMER1 mtm
                        where
                        tc.objid=TSP.SITE_PART2CUSTOMER
                        and bus.objid=MTM.BUS_ORG2CUSTOMER
                        and tc.objid=MTM.CUSTOMER2BUS_ORG
                        and tc.type='Residential Customer'
                        --and TSP.PART_STATUS='Active'
                        and TC.X_IS_EXTERNAL='0'
                        and X_IDENTITY_NUMBER = '{cnp}'
                       AND tsp.instance_name = 'Mobile'
                        group by X_IDENTITY_NUMBER, PART_STATUS"""

        return self.exec_Select(self.crm_Conn.db_conn, query=query)

    def standardize_input(self, input_Dictionary):
        trusted_keys = ['customerType', 'master', 'status', 'get_full']
        for key in trusted_keys:
            if key not in input_Dictionary.keys():
                input_Dictionary[key] = 'false'
        return input_Dictionary

    def set_potential_results(self):
        for dictionary in self.input_List:
            self.result[f"{dictionary['customerType']}_{dictionary['master']}_{dictionary['status']}"] = []

    def get_customerInfo(self, cnp):
        query = f"""select X_IDENTITY_NUMBER, customer_id, tc.NAME
                    from
                    table_customer tc,
                    table_site_part tsp,
                    table_bus_org bus,
                    MTM_BUS_ORG95_CUSTOMER1 mtm
                    where
                    tc.objid=TSP.SITE_PART2CUSTOMER
                    and bus.objid=MTM.BUS_ORG2CUSTOMER
                    and tc.objid=MTM.CUSTOMER2BUS_ORG
                    --and tc.type='Residential Customer'
                    --and TSP.PART_STATUS='Cancelled'
                    --and TC.X_IS_EXTERNAL='0'
                    and X_IDENTITY_NUMBER = '{cnp}'"""

        return self.exec_Select(self.crm_Conn.db_conn, query)

    def create_insert_statement(self, custInfo):
        insrt = f"""insert into iods.NP_CUSTOMER (EDW_CUSTOMER_ID, SSN_CUI, CUSTOMER_ID, FIRST_NAME, LAST_NAME, CUSTOMER_TYPE, BILLING_ADDRESS_ID, SOCIAL_ADDRESS_ID, FIRST_ACTIVATION_DATE, CUSTOMER_ROLE, CUSTOMER_ROLE_DATE, SUSPENDED_FLAG, SUSPENDED_FLAG_DATE, CUSTOMER_CONTRACT_END_DATE, DATE_FROM, DATE_TO, CREATION_DATE, CREATED_BY, UPDATE_DATE, UPDATED_BY, SRC_UPDATE_DATE, PK_HASH, MSISDN_VDF_LP, MSISDN_VDF_LP_INSERT_DATE, EMAIL, PHONE_CLEAN, C_PHONE, C_X_MPHONE, C_X_PROFPHONE, ACCOUNT_CATEGORY, COMPANY_NAME)
                values ('189702', '{custInfo['X_IDENTITY_NUMBER']}', '{custInfo['CUSTOMER_ID']}', 'CATALINA ELENA', '{custInfo['NAME'].split(' ')[-1]}', 'CBU', null, null, to_date('03-09-2018 16:37:46', 'dd-mm-yyyy hh24:mi:ss'), 'Active', null, null, null, to_date('20-06-2020 18:42:23', 'dd-mm-yyyy hh24:mi:ss'), to_date('05-02-2020', 'dd-mm-yyyy'), to_date('31-12-9999', 'dd-mm-yyyy'), '05-FEB-20 01.24.51.000000 PM', 'ETL', '05-FEB-20 01.24.51.000000 PM', 'ETL', to_date('05-02-2020', 'dd-mm-yyyy'), '0004A548D04AAAD1E0C4C860423C355A', null, null, null, '0722697444', '0722697444', null, null, 'Residential', null)
                """
        return insrt

    def set_common_customer(self, custInfo):
        header = {'X-Srcsystem': 'Automated_Data_Prep', 'X-External-Id': custInfo['X_IDENTITY_NUMBER']}
        body = {
               "NP-AccountId":f"{custInfo['CUSTOMER_ID']}",
               "ConfirmationStatus":"notCommon",
               "SSN":f"{custInfo['X_IDENTITY_NUMBER']}",
               "CustomerId":f"{custInfo['CUSTOMER_ID']}",
               "NP-LastName":f"{custInfo['NAME'].split(' ')[-1]}",
               "AgentName":f"automation_user_dummy",
               "CalculateStatus":"Yes"
                }
        rsp = requests.post(url='http://bv-eai-uat5.connex.ro:7083/ods/services/setCommonCustomer',
                      data=json.dumps(body), header=header)
        return rsp.text

    def main(self):
        self.set_potential_results()
        for input_Dictionary in self.input_List:
            input_Dictionary = self.standardize_input(input_Dictionary)
            self.load_data(input_Dictionary)
            self.identify_RequiredCustomer(input_Dictionary)
        #
        # for key, val in self.result.items():
        #     if 'notcommon' not in key:
        #         if len(val) == 0:
        #             if len(self.result[key.replace('Common', 'notcommon')]) >=2:
        #                 custInfo = (self.get_customerInfo(self.result[key.replace('Common', 'notcommon')][1]['CNP']))
        #                 self.exec_Insert(query=self.create_insert_statement(custInfo[0]), conn=self.ods2_IODS.db_conn)
        #                 self.result[key.replace('Common', 'notcommon')].remove(self.result[key.replace('Common', 'notcommon')][1]['CNP'])
        #                 self.set_common_customer(custInfo[0])
        #                 self.result[key].append({f'CNP': custInfo[0]["X_IDENTITY_NUMBER"]})


        self.crm_Conn.db_conn.close()
        self.ods_SELONLY.db_conn.close()
        self.ods_IODS.db_conn.close()
        return self.result


if __name__ == "__main__":
    demo =\
        [{
        "customerType": "Common",
        "master": "OMS",
        "status": "all_Cancelled",
        # 'get_full': 'true'
        },
        {
        "customerType": "notcommon",
        "master": "OMS",
        "status": "all_Cancelled",
        'get_full': 'true'
        },
        {
        "customerType": "notcommon",
        "master": "OMS",
        "status": "Active",
        # 'get_full': 'true'
        },
        {
        "customerType": "Common",
        "master": "OMS",
        "status": "Active",
        # 'get_full': 'true'
        }
        ]

    d = [{'customerType': 'NewCustomer', 'master': 'OMS', 'status': 'New_Customer'}]
    setCustomer = common_NotCommon_Customer(demo, 123)
    print(setCustomer.main())
    # print(demo)
