import os
import sys
import paramiko
from os.path import expanduser
import zipfile

class ReservationId:
    log_dir = os.path.dirname(os.path.realpath(__file__)) + '/dex_log'
    log_file = ''
    def __init__(self, identification_number, hostname):
        self.identification_number = identification_number
        self.hostname = hostname

    def connect_to_host(self):
        home = expanduser("~")
        print(home)
        conf = paramiko.SSHConfig()
        # print(os.path.expanduser(home+'/.ssh/config'))
        conf.parse(open(home + '/.ssh/config'))
        # conf.parse(open("C:/Users/MUNTEA~1/ONEDRI~1/DOCUME~1/MOBAXT~1/home/.ssh/config"))
        host = conf.lookup(self.hostname)
        client = paramiko.SSHClient()
        client.load_system_host_keys()
        print(host)
        # client.set_missing_host_key_policy('AutoAddPolicy')

        client.connect(
            host['hostname'], username=host['user'],
            key_filename=host['identityfile'],
            banner_timeout=30, auth_timeout=10,
            sock=paramiko.ProxyCommand(host.get('proxycommand')))


        sftp_client = client.open_sftp()
        fileName = self.get_file_name(client, '8912343213')
        remote_file = sftp_client.open(f'/users/gen/dexpwrk1/DIGITAL/logs/{fileName}')
        self.get_file_name(client, fileName)
        self.get_log_file(remote_file, fileName)
        print(self.extract_id())
        remote_file.close()

    def get_file_name(self, client, indentifing_number, count=0, command="grep -li 'placeHolder' *"):
        command = command.replace('placeHolder', indentifing_number)
        stdin, stdout, stderr = client.exec_command(command)
        r_stdout = stdout.read().decode("utf-8").rstrip()

        if not len(r_stdout):
            return r_stdout
        else:
            if count == 0:
                self.get_file_name(client, indentifing_number, count=1, command=f"zgrep -li '{indentifing_number}' *")


    def get_log_file(self, remote_file, fileName):

        if 'zip' not in fileName.lower():
            try:
                os.mkdir(self.log_dir)
            except FileExistsError:
                pass
            with open(self.log_dir + "log.txt", 'w') as file:
                file.write(str(remote_file))
        else:
            with zipfile.ZipFile(remote_file, 'r') as zip_ref:
                zip_ref.extractall(self.log_dir)

        self.log_file = (list(os.walk(self.log_dir))[0][-1][0])

    def extract_id(self):
        sessionId = ''
        with open(self.log_dir + '\\' + self.log_file, 'r') as log:
            log_list = log.read().split('DEBUG]')
        for element in log_list:
            if self.identification_number in element:
                # print(element)
                for i in element.split(']['):
                    if 'sess' in i:
                        sessionId = i.split(':')[-1]
                        break
        for element in log_list:
            if sessionId in element:
                if 'reservationid' in element.lower():
                    for j in element.split("<com.vodafone.api.eai.productStockReservation.model.IDType>"):
                        if 'reservationid' in j.lower():
                            return (j.split('</value>')[0].replace('<value>', '').strip())

if __name__ == "__main__":
    # getToken('dex2')
    resId = ReservationId('55552347', 'dex2')
    resId.connect_to_host()
    # resId.get_log_file('', 'connectors_9.log.zip')
    # resId.extract_id()