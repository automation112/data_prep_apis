ANDREIGH_LOG = {'ip': "10.230.167.129",'port': "1524",'SID': "EDWTST", 'usr': "andreigh",'pass':"uat123"}


def get_ConnDetails(env):

    db_conn_string = {
                        "CRM": {
                                "CRM30": {
                                    "host": "alesi",
                                    "port": '1523',
                                    "serviceName": "VFRCRM30",
                                    "user": "SA",
                                    "password": "SA"
                                },
                                "CRM20": {
                                    "host": "alesi",
                                    "port": '1523',
                                    "serviceName": "VFRCRM20",
                                    "user": "SA",
                                    "password": "SA"
                                }
                        },
                        "CM": {
                                "CM30": {
                                    "host": "roabp7dr",
                                    "port": '15211',
                                    "serviceName": "ABPUAT",
                                    "user": "VFRAPP30",
                                    "password": "VFRAPP30"
                                },
                                "CM20": {
                                    "host": "roabp7dr",
                                    "port": '15211',
                                    "serviceName": "ABPUAT",
                                    "user": "VFRAPP20",
                                    "password": "VFRAPP20"
                            }
                        }
    }

    dexEnv = {
                'dexuat1':
                            {
                             "CM_ENV": "CM30",
                             "EAI": "EAIUAT5",
                              "URL": "https://dexuat1.aws.connex.ro/retail/login",
                              "PEGA_ENV": "PEGA_UAT3",
                              "VOS": "SIM2"
                            },
                'dexuat2':
                            {
                             "CM_ENV": "CM20",
                             "EAI": "EAIUAT6",
                             "URL": "https://dexuat2.aws.connex.ro/retail/login",
                             "PEGA_ENV": "PEGA_UAT2",
                             "VOS": "SIM1"}
    }

    if 'cm' in env.lower():
        return db_conn_string['CM'][env]
    elif 'crm' in env.lower():
        return db_conn_string['CRM'][env]
    elif 'dexuat' in env.lower():
        return dexEnv[env]

if __name__ == '__main__':
    print(get_ConnDetails('CRM20')['host'])