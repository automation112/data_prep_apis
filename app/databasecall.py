from oracle import Oracle
import random
import logging


class DatabaseCall():
    def __init__(self, no_rec, s_method, flow_type,env):
        self.no_rec = no_rec
        self.s_method = s_method
        self.flow_type = flow_type
        self.env = env

    def getSubsList(self):
        in_env = "ANDREIGH"
        subs_sql="select subs from (select distinct(subscriber_cm)subs from table_common_customer where  env=%s )ORDER BY DBMS_RANDOM.RANDOM"%self.env
        iter_list = Oracle(subs_sql, int(
            self.no_rec), 'dummy', in_env)
        iter_res = iter_list.GetTesetResult()
        ret_lst = []
        for f in iter_res:
            subs_key = f
            ret_lst.append(subs_key[0])
        return ret_lst

    def getPosibleMethods(self):
        in_env = "ANDREIGH"
        method_sql = "select distinct(t.scenario) from table_common_request t where lower(t.flow_type) ='%s' and t.version is not null  "%self.flow_type
        get_methods = Oracle(method_sql, int(
            self.no_rec), 'dummy', in_env)
        availbile_m = get_methods.GetTesetResult()
        met_list = []
        for f in availbile_m:
            method = f
            met_list.append(method[0])
        return met_list


    def getReqTemp(self):
        in_env = "ANDREIGH"
        sql_prepared = """select t.request from TABLE_COMMON_REQUEST t where t.scenario='%s' and lower(t.flow_type)='%s' and t.version is not null""" % (self.s_method,self.flow_type)
        iter_req = Oracle(sql_prepared, 1, 'dummy', 'ANDREIGH_LOG')
        request_str = iter_req.getClob()
        return request_str

    def getAnyQuery(self):
        in_env = "ANDREIGH"
        sql_prepared = str(self.s_method)
        iter_req = Oracle(sql_prepared, 1, 'dummy', 'ANDREIGH_LOG')
        request_str = iter_req.getClob()
        return request_str



    def getNonClobQuery(self):
        in_env = "ANDREIGH"
        sql_prepared = str(self.s_method)
        iter_req = Oracle(sql_prepared, 1, 'dummy', 'ANDREIGH_LOG')
        request_str = iter_req.getQueryResult()
        return request_str

    def getSubsListTuple(self):
        in_env = "ANDREIGH"
        subs_sql="select subscriber_cm,nr_pega from (select distinct subscriber_cm,nr_pega from table_common_customer where  env=%s )ORDER BY DBMS_RANDOM.RANDOM"%self.env
        iter_list = Oracle(subs_sql, int(
            self.no_rec), 'dummy', in_env)
        iter_res = iter_list.GetTesetResult()
        return iter_res
    
    def getICCID(self):
        in_env = "ANDREIGH"
        print(self.env)

        iccid_sql= self.s_method
        output = Oracle(iccid_sql, int(self.no_rec), 'dummy', in_env)
        out_iccid = output.GetTesetResult()
        iccid_list = []
        for f in out_iccid:
            iccid = f
            iccid_list.append(iccid[0])
        return iccid_list

    def getSubId(self):
        in_env = "ANDREIGH"
        subid_sql="select subscriber_cm  from table_common_customer t where t.nr_cm=%s"%self.env
        output=Oracle(subid_sql,int(self.no_rec), 'dummy', in_env)
        out_sub=output.GetTesetResult()
        for f in out_sub:
            returned_sub=f
        return returned_sub

    def getPrepaid(self):
        in_env = "ANDREIGH"
        iccid_sql="""select rm.resource_value
          from rm1_resource@Cm20.new rm
           where rm.resource_status = 'MIGRATE_TO_PREPAID' and rm.resource_value like '7%'"""
        output = Oracle(iccid_sql, int(self.no_rec), 'dummy', in_env)
        out_prepaid = output.GetTesetResult()
        prepaid_list = []
        for f in out_prepaid:
            pre = f
            prepaid_list.append(pre[0])
        return prepaid_list

    def updateRM1Res(self):
        in_env = "ANDREIGH"
        update_sql = """update rm1_resource@Cm20.new
        set RESOURCE_STATUS = 'ASSIGNED', LAST_ACTIVITY_NAME = 'RELEASE'
        where RESOURCE_TYPE_ID = 8
        and RESOURCE_VALUE = :1
        and RESOURCE_STATUS like 'TMP_RESERVED'"""
        output = Oracle(sql=update_sql, no_rec=int(self.no_rec), fn=tuple([self.env]),in_env=in_env)
        result = output.UpdateRow()
        return(result)
    
    def insertSSN(self):
        in_env = "ANDREIGH_LOG"
        insert_sql = f"""insert into bdp.CR_SSN_RISK_SCORING@{self.no_rec} (SSN, CREDIT_LIMIT, COLLECTION_PLAN, UPDATE_DATE, CR_MAX, SSN_STATUS, CUST_ACTIVATION_DATE)
values (:ssn, '11.45', '0M', to_date('21-11-2019 17:58:47', 'dd-mm-yyyy hh24:mi:ss'), :cr_amt, 'VOLUNTAR', null)"""
        logging.info(insert_sql)
        output = Oracle(sql=insert_sql,no_rec=1,fn=tuple([self.s_method,self.flow_type]),in_env='ANDREIGH_LOG')
        result = output.insert()
        return result




if __name__ == '__main__':
    # db = DatabaseCall(10,'024_Achizitie_Franciza_MNP_PREPAY_SERVICES','franciza',20).getReqTemp()
    # db2 = DatabaseCall(10,'024_Achizitie_Franciza_MNP_PREPAY_SERVICES','franciza',20).getPosibleMethods()
    # db3 = DatabaseCall(10,'024_Achizitie_Franciza_MNP_PREPAY_SERVICES','franciza',20).getSubsListTuple()
    # db4 = DatabaseCall(10,'024_Achizitie_Franciza_MNP_PREPAY_SERVICES','franciza',20).getICCID()
    db5 = DatabaseCall(1,'1940609239911','2000','ANDREIGH_LOG').insertSSN()
    print(db5)
  
