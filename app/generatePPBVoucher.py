import requests

class voucherAction():
    def __init__(self, url, voucherSerialNumber='', amount=0):
        self.amount = amount
        self.voucherSerialNumber = voucherSerialNumber
        self.url = url

    def generateVoucher(self):
        return
    def confirmVoucher(self):
        return
    def checkVoucherData(self):
        # url="http://wsf.cdyne.com/WeatherWS/Weather.asmx?WSDL"
        headers = {'content-type': 'application/soap+xml'}
        # headers = {'content-type': 'text/xml'}
        body = f"""<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:srs="http://vodafone.ro/eai/SRSService">
                    <soapenv:Header/>
                    <soapenv:Body>
                        <srs:getVoucherDataReq>
                            <header>
                            </header>
                            <UserName>middleware</UserName>
                            <Password>eai2sicap1</Password>
                            <VoucherSerialNumber>{self.voucherSerialNumber}</VoucherSerialNumber>
                        </srs:getVoucherDataReq>
                    </soapenv:Body>
                    </soapenv:Envelope>
                """
        response = requests.post(self.url, data=body, headers=headers)
        if response == 200:
            return response.content
        else:
            return f"Failed response from {self.url}, http_code: {response}"

if __name__ == '__main__':
    print(voucherAction(voucherSerialNumber='0005563300000015', url='http://bv-eai-uat4:7810/eai/services/SRSService').checkVoucherData())
    print('main?')