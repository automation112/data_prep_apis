from openpyxl import load_workbook
import pandas as pandasLib
import os
import pathlib
import datetime

class ExecutionDetails:

    def __init__(self, testExecutionDictionary):
        self.testExecutionDictionary = testExecutionDictionary['testExec']
        self.directoryPath = pathlib.Path().absolute()
        self.paramsDict = testExecutionDictionary['params']

    def getExcelFileNamePath(self):
        if 'nt' in os.name.lower():
            name = f"ExecutionDetails_ALM_ID_NoneGiven"
        else:
            name = f"""ExecutionDetails_ALM_ID_{self.getDictionaryValue("Release", self.testExecutionDictionary).split("_")[-1]}"""
        return f'{self.directoryPath}/Reports/{name}.xlsx'

    def createExcelFile(self, excelFileNamePath):
        if os.path.isfile(excelFileNamePath) is False:
            # dataframe Name and Age columns
            dataFrame = pandasLib.DataFrame({'Data Rularii': [],
                               'Release': [],
                                'System Name': [],
                                'Environment': [],
                                'Functionalitatea':[],
                                'Numele Testului': [],
                                'Parametrii de rulare':[],
                                'Status': [],
                                'DefectId':[]})

            # Create a Pandas Excel writer using XlsxWriter as the engine.
            writer = pandasLib.ExcelWriter(f'{excelFileNamePath}', engine='xlsxwriter')

            # Convert the dataframe to an XlsxWriter Excel object.
            dataFrame.to_excel(writer, sheet_name='Execution Details', index=False)

            # Close the Pandas Excel writer and output the Excel file.
            writer.save()
            print(f'File created {excelFileNamePath}')
        else:
            print(f'File already created {excelFileNamePath}')

    def appendToExcelFile(self, excelFileNamePath, excelData):
        dataFrame = pandasLib.DataFrame(excelData)
        writer = pandasLib.ExcelWriter(excelFileNamePath, engine='openpyxl')
        # try to open an existing workbook
        writer.book = load_workbook(excelFileNamePath)
        # copy existing sheets
        writer.sheets = dict((ws.title, ws) for ws in writer.book.worksheets)
        # read existing file
        reader = pandasLib.read_excel(excelFileNamePath, engine='openpyxl')
        # write out the new sheet
        dataFrame.to_excel(writer, sheet_name='Execution Details', index=False, header=False, startrow=len(reader) + 1)
        writer.close()

    def getParamsFromExecLog(self):
        outputJson = {'Data Rularii': [self.convertDate(f'{self.getDictionaryValue("EXECUTION_TS", self.testExecutionDictionary)}')],
                     'Release': [f'{"_".join(self.getDictionaryValue("Release", self.testExecutionDictionary).split("_")[:-1])}'],
                     'System Name': ['DEX'],
                     'Environment': [self.paramsDict['dexEnv']],
                     'Functionalitatea': [self.concatFunctionalitate()],
                     'Numele Testului': [list(self.testExecutionDictionary.keys())[0]],
                     'Parametrii de rulare': [self.paramsDict],
                     'Status': [f'{self.getDictionaryValue(f"Status", self.testExecutionDictionary)}'],
                     'DefectId': ['TBD']}
        # print('\n\n\n\n', self.getDictionaryValue("Release", self.testExecutionDictionary).split("_"))
        # print(outputJson)
        return outputJson

    def getDictionaryValue(self, key, dictionary):
        try:
            return dictionary[list(self.testExecutionDictionary.keys())[0]][key]
        except Exception:
            return 'Not found'

    def concatFunctionalitate(self):
        concatDict = {'primary': ['channel', 'searchType', 'configType', 'abNouType'],
                      'secondary': ['scenarioNameDC', 'tipServiciu', 'scenarioName_PageSearch', 'offerChoice', 'scenarioName_PC']}

        primaryString = ''
        secondaryString = ''
        for key, value in concatDict.items():
            for i in value:
                if key == 'primary':
                    primaryString = primaryString + '_' + self.getDictionaryValueNull(i, self.paramsDict)
                else:
                    secondaryString = secondaryString + '_' + self.getDictionaryValueNull(i, self.paramsDict)

        return ("|" + primaryString + '|' + secondaryString + '|').replace('|_', '|').replace('precbu_', '')

    def getDictionaryValueNull(self, key, dictionary):
        try:
            return dictionary[key]
        except Exception:
            return ''

    def convertDate(self, date):
        #1
        year = int(date[0:4])
        month = int(date[4:6])
        day = int(date[6:8])
        mlist = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October',
                 'November', 'December']
        t = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
        r = datetime.datetime.today().weekday()
        return  (f'{year}, {mlist[month - 1]}, {day} ({t[r]})')

    def appendTxt(self, path, data):
        path = path.replace('xlsx', 'txt')
        with open(path, 'a') as opened_file:
            opened_file.write(str(data) + ',')

    def main(self):

        excelFileNamePath = self.getExcelFileNamePath()
        # self.createExcelFile(excelFileNamePath=excelFileNamePath)
        #
        excelData = self.getParamsFromExecLog()
        # self.appendToExcelFile(excelFileNamePath=excelFileNamePath, excelData=excelData)
        self.appendTxt(excelFileNamePath, excelData)



if __name__ == '__main__':
    
    json = {
        "Test_Name": {
            "Status": "PASSED",
            "HOST Name": "LocalHost TBD",
            "Release": "R05V29_00_23632",
            "ConnectorLog": "C:\\Users\\MunteanuG\\PycharmProjects\\versionControl\\dex\\Output\\TraceFiles\\None.txt",
            "EXECUTION_TS": "20201216133326",
            "Tests Execution Log": {
                "test_page_Login": {
                    "Status": "PASSED",
                    "ExecutionLog": {
                        "Click_OK": "PASSED",
                        "Type_UserName": "PASSED",
                        "Type_Password": "PASSED",
                        "Click_Login": "PASSED",
                        "Select by index_DealerSelect": "PASSED",
                        "Click_Continua": "PASSED"
                    }
                }
            }
        }
    }

    json={'t1': {'Status': 'PASSED', 'HOST Name': 'LocalHost TBD', 'Release': 'V19 From PyCharm Env', 'ConnectorLog': 'C:\\Users\\MunteanuG\\PycharmProjects\\versionControl\\dex\\Output\\TraceFiles\\770580240.txt', 'EXECUTION_TS': '20201217102756', 'Tests Execution Log': {'test_page_Login': {'Status': 'PASSED', 'ExecutionLog': {'Click_OK': 'PASSED', 'Type_UserName': 'PASSED', 'Type_Password': 'PASSED', 'Click_Login': 'PASSED', 'Select by index_DealerSelect': 'PASSED', 'Click_Continua': 'PASSED'}}, 'test_page_Search': {'Status': 'PASSED', 'ExecutionLog': {'Click_MSISDN': 'PASSED', 'Type_searchKey_MSISDN': 'PASSED', 'Click_cauta': 'PASSED'}}}}}

#tst123
    params = { 'channel': 'RETAIL',
 'searchType': 'MSISDN',
 'searchValue': 722486663,
 'configType': 'precbu_ACHIZITIE',
 'abNouType': 'MNP',
 'tipServiciu': 'PREPAID',
 'scenarioNameDC': 'CR_False_Client_nonMatur',
 'offerChoice': 'PachetPromo',
 'scenarioName_PC': 'BundleOffers_SimOnly_si_Discount',
 'tipContract': 'permanent',
 'scenarioName_Utl_Nr': 'TipContract_Permanet_Numar_Special',
 'scenarioName_SendNotif': 'Anuleaza_Change_Notif_Number',
 'scenarioName_isTitular': 'Utilizator_titular_No_Minor_Yes',
 'scenarioName_GDPR': 'Scenariu_GDPR_Minor_Y',
 'scenarioContNou': 'Is_Cont_Nou_Required_Yes_FacturaElectr',
 'webAppVersion': 'precbu',
 'pega_Env': 'pega_uat1',
    'dexEnv': 'asd'}


    inputData = {'testExec': json,
                 'params': params}

    # inputData = {'testExec': {'t1': {'Status': 'Invalid_TestExecution', 'HOST Name': 'LocalHost TBD', 'Release': 'V19 From PyCharm Env', 'ConnectorLog': 'C:\\Users\\MunteanuG\\PycharmProjects\\versionControl\\dex\\Output\\TraceFiles\\281281442.txt', 'EXECUTION_TS': '20201217103615', 'Tests Execution Log': {'test_page_Login': {'Status': 'PASSED', 'ExecutionLog': {'Click_OK': 'PASSED', 'Type_UserName': 'PASSED', 'Type_Password': 'PASSED', 'Click_Login': 'PASSED', 'Select by index_DealerSelect': 'PASSED', 'Click_Continua': 'PASSED'}}, 'test_page_Search': {'Status': 'PASSED', 'ExecutionLog': {'Click_MSISDN': 'PASSED', 'Type_searchKey_MSISDN': 'PASSED', 'Click_cauta': 'PASSED'}}}, 'FAULT': "Invalid_TestExecution_REASON --> [Execution steps not fulfilled => [['test_page_configureaza', 'test_page_detalii_UtilizatorTitular', 'test_page_Dashboard', 'test_page_SendToVOS', 'test_page_Cont_Nou', 'test_page_detalii_Utilizator_Si_Numar', 'test_page_Detail_GDPR']]]"}}, 'params': {'channel': 'RETAIL', 'searchType': 'MSISDN', 'searchValue': 733044811, 'configType': 'precbu_ACHIZITIE', 'abNouType': 'MFP', 'scenarioNameDC': 'CR_True_Client_Matur', 'offerChoice': 'PachetPromo', 'scenarioName_PC': 'BundleOffers_Rate', 'scenarioName_Utl_Nr': 'Use_existing_SIM_card_No', 'scenarioName_isTitular': 'Utilizator_titular_No_Minor_No', 'scenarioName_GDPR': 'Scenariu_GDPR_allYES', 'scenarioContNou': 'Is_Cont_Nou_Required_Yes_FacturaElectr', 'webAppVersion': 'precbu', 'pega_Env': 'pega_uat1'}}


    print(inputData)
    for i in range(20):
        writeToExcel = ExecutionDetails(inputData)
        writeToExcel.main()