import cx_Oracle

class CaponeActions():
    def __init__(self,ssn):
        self.ssn = ssn 

    def connection(self):

        ip = 'roilk5hr'
        port = '1521'
        SID = 'CAPUAT1'
        DNS_TNS = cx_Oracle.makedsn(ip, port, SID)
        db = cx_Oracle.connect('captelco','Collection111!',DNS_TNS)
        return db

    def getCaponeStatus(self):
        db = self.connection()
        cur = db.cursor()
        Po_Device_Bd = cur.var(int)
        Po_Service_Bd= cur.var(int)
        Po_Black_List= cur.var(int)
        Po_Coll_Restriction= cur.var(int)
        Po_Cust_Status= cur.var(str)
        Po_Cursor = db.cursor()
        Po_Cust= cur.var(int)
        Po_Coll_Plan = cur.var(str)
        Po_Response= cur.var(int)
        Po_Message= cur.var(str)
        cur.callproc('interfaces_out_pkg.bad_debt_check',[self.ssn,'All',Po_Device_Bd,Po_Service_Bd,Po_Black_List,Po_Coll_Restriction,Po_Cust_Status,Po_Cursor,Po_Cust,Po_Coll_Plan,Po_Response,Po_Message])
        cur.close()
        db.close()
        return(Po_Coll_Restriction.getvalue())

    def updateCapone(self):

        status = self.getCaponeStatus()
        if status == 1:            
            db = self.connection()
            cur = db.cursor()
            sql = 'select count(*) from customers where ssn = :ssn'
            query = cur.execute(sql,[self.ssn])
            output = cur.fetchmany(1)
            if output[0][0] >= 1 and output[0][0] <= 20:
                update_stmt = "update customers set delay= null, restriction='N' where ssn= :ssn"
                cur.execute(update_stmt,[self.ssn])
                db.commit()
                row_cnt = str(cur.rowcount)
                new_status = self.getCaponeStatus()
                return(str(row_cnt) + ' Rows updated for SSN: ' + self.ssn + ' and Capone status is now ' + str(new_status))
            else:
                return(f'too many rows {output[0][0]}for update, pls check with capone')
            cur.close()
            db.close()
        else:
            return(f'For {self.ssn} status is Not Debt, no update required')

if __name__ == '__main__':
    print(CaponeActions(ssn='RO16645658').checkBadDebt())
