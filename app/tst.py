import os
import subprocess

def getLog():
    import sys
    identifier = sys.argv[1]
    os.chdir('/users/gen/dexpwrk1/DIGITAL/logs/')
    fileName = subprocess.check_output("grep -il '671230823' *", shell=True)
    fileName = fileName.split(" ")[-1]
    f = open(fileName.replace('\n', ''), "r")
    return str(f.read())


if __name__ == '__main__':
    getLog()
