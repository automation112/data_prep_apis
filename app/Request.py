import json

class RequestPrepare():
    def __init__(self, in_json, in_params):
        self.in_json = in_json
        self.in_params = in_params


    def prepareReq(self):
        prepared=json.loads(self.in_json % self.in_params)
        return prepared
