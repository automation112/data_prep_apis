import cx_Oracle


class database_Connection:

    db_conn = None

    def __init__(self, host, port, serviceName, user, password):
        self.host = host
        self.port = port
        self.serviceName = serviceName
        self.user = user
        self.password = password
        self.connect_to_db()

    def connect_to_db(self):
        dsn_tns = cx_Oracle.makedsn(self.host, self.port,
                                    service_name=self.serviceName)  # if needed, place an 'r' before any parameter in order to address special characters such as '\'.
        self.db_conn = cx_Oracle.connect(user=self.user, password=self.password,
                                 dsn=dsn_tns)  # if needed, place an 'r' before any parameter in order to address special characters such as '\'. For example, if your user name contains '\', you'll need to place 'r' before the user name: user=r'User Name'

    def exec_Select(self, query):
        results = []
        cursor = self.db_conn.cursor()
        cursor.execute(query)  # use triple quotes if you want to spread your query across multiple lines
        columns = [column[0] for column in cursor.description]

        for row in cursor.fetchall():
            results.append(dict(zip(columns, row)))

        return results

    def exec_update(self, query):
        results = []
        cursor = self.db_conn.cursor()
        cursor.execute(query)  # use triple quotes if you want to spread your query across multiple lines
        result = cursor.rowcount
        self.db_conn.commit()
        return result

    def exec_Insert(self, query):
        cursor = self.db_conn.cursor()
        cursor.execute(query)  # use triple quotes if you want to spread your query across multiple lines
        print(cursor.rowcount())