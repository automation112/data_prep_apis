from databasecall import DatabaseCall
import requests
import json
import os
from Request import RequestPrepare

class Subscribers():
    def __init__(self, int_no_subs, sub_list, template, user, storeid):
        self.int_no_subs = int_no_subs
        self.sub_list = sub_list
        self.template = template
        self.user = user
        self.storeid = storeid


    def getCommonSubscriber(self):
        #env_url = os.environ['ENV_URL']
        env_url = 'http://bv-eai-uat5.connex.ro:7080/eai/services/customerMarketingProduct/search'
        d = {}
        for f in self.sub_list:
            abc=f[0]
            data = json.loads(self.template %{"subscriber":f[0],"username":self.user,"storeid":self.storeid})
            print(data)
            headers = {'X-SrcSystem': 'EAI', 'X-Transaction-Id': '12345',
                       'X-Correlation-Id': 'TestEAI', 'Content-Type': 'WS1013'}
            response = requests.post(env_url, json=data, headers=headers)
            if response.status_code == 200:
                d[f[1]] = response.status_code
                if (len(d) == self.int_no_subs):
                    break
            if response.status_code != 200:
                print(str(f) + 'not found')
                d[f[1]] = 'Not found'
                if (len(d) == self.int_no_subs):
                    break
        return(d)


if __name__ == '__main__':
    req = DatabaseCall(10,'022_Franciza_MNP_PREPAY_MOBILE_WITH_DISCFMC','franciza',20).getReqTemp()
    db3 = DatabaseCall(10,'022_Franciza_MNP_PREPAY_MOBILE_WITH_DISCFMC','store',20).getSubsListTuple()
    print(db3)

    a = Subscribers(3,db3,req,'mihaela.filip',1043).getCommonSubscriber()
    print(a)

