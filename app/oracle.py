# -*- coding: utf-8 -*-
"""
Created on Fri Feb 15 16:27:24 2019

@author: Enea1
"""
import cx_Oracle
import csv
from time import gmtime, strftime
import constants
import json
import time


class Oracle():
    def __init__(self, sql, no_rec, fn, in_env):
        self.sql = sql
        self.no_req = no_rec
        self.fn = fn
        self.in_env = in_env

    def GetTesetResultJSON(self):
        env = self.in_env + '_LOG'
        print(env)
        con_str = getattr(constants,env)
        print(con_str)
        ip = con_str["ip"]
        port = con_str["port"]
        SID = con_str["SID"]
        usr = con_str["usr"]
        pas = con_str["pass"]
        DNS_TNS = cx_Oracle.makedsn(ip, port, SID)
        db = cx_Oracle.connect(usr,pas,DNS_TNS)
        cur = cursor.var(cx_Oracle.LONG_STRING, arraysize=cursor.arraysize)
        cur = db.cursor()
        cur.execute(self.sql)
        output = cur.fetchall()


        cur.close()
        db.close()
        return json

    def makedatafile(self):
        con_str=getattr(constants,self.in_env)
        ip = con_str["ip"]
        port = con_str["port"]
        SID = con_str["SID"]
        DNS_TNS = cx_Oracle.makedsn(ip, port, SID)
        try:
            db = cx_Oracle.connect('iods','passw0rd',DNS_TNS)
            cur = db.cursor()
            print("DB Connection established on: "+DNS_TNS)
            query = cur.execute(self.sql)
            output = cur.fetchmany(self.no_req)
            cols = []
            date = strftime("%Y%m%d%H%M", gmtime())
            for x in cur.description:
                cols.append(x[0])
                file_name=self.fn+'_'+date+'.csv'
                fp = open(file_name, 'w')
                myfile = csv.writer(fp, lineterminator='\n')
                myfile.writerow(cols)
                myfile.writerows(output)
                fp.close()
        except (KeyboardInterrupt, SystemExit):
            cur.close()
            db.close()
            print("Oracle Connection closed by System Interrupt/Keyboard")
        cur.close()
        db.close()
        return file_name

    def GetTesetResult(self):
        env = self.in_env + '_LOG'
        print(env)
        con_str = getattr(constants,env)
        print(con_str)
        ip = con_str["ip"]
        port = con_str["port"]
        SID = con_str["SID"]
        usr = con_str["usr"]
        pas = con_str["pass"]
        DNS_TNS = cx_Oracle.makedsn(ip, port, SID)
        db = cx_Oracle.connect(usr,pas,DNS_TNS)
        cur = db.cursor()
        query = cur.execute(self.sql)
        output = cur.fetchmany(self.no_req)
        cur.close()
        db.close()
        return output

    def InsertInto(self):
        env = self.in_env + '_LOG'
        print(env)
        con_str = getattr(constants,env)
        print(con_str)
        ip = con_str["ip"]
        port = con_str["port"]
        SID = con_str["SID"]
        usr = con_str["usr"]
        pas = con_str["pass"]
        DNS_TNS = cx_Oracle.makedsn(ip, port, SID)
        db = cx_Oracle.connect(usr,pas,DNS_TNS)
        rows=self.sql
        cur=db.cursor()
        cur.bindarraysize=7
        cur.setinputsizes(int,self.no_req)
        cur.executemany("INSERT INTO sys_rls_nple_uat (release,environment)VALUES(:1,:2)", rows, batcherrors=True)
        for error in cur.getbatcherrors():
            print("Error", error.message, "at row offset", error.offset)
        db.commit()
        cur.close()
        db.close()

    def UpdateRow(self):
        env = self.in_env + '_LOG'
        print(env)
        con_str = getattr(constants,env)
        print(con_str)
        ip = con_str["ip"]
        port = con_str["port"]
        SID = con_str["SID"]
        usr = con_str["usr"]
        pas = con_str["pass"]
        DNS_TNS = cx_Oracle.makedsn(ip, port, SID)
        db = cx_Oracle.connect(usr,pas,DNS_TNS)
        cur=db.cursor()
        cur.execute(self.sql,self.fn)
        db.commit()
        row_cnt = str(cur.rowcount)
        cur.close()
        db.close()
        return(row_cnt)


    def CallPrc(self):
        con_str=getattr(constants,self.in_env)
        ip = con_str["ip"]
        port = con_str["port"]
        SID = con_str["SID"]
        DNS_TNS = cx_Oracle.makedsn(ip, port, SID)
        try:
            db = cx_Oracle.connect('cwr04','passw0rd',DNS_TNS)
            cur = db.cursor()
            print("DB Connection established on: "+DNS_TNS)
            query = cur.execute(self.sql)
            output = cur.fetchmany(self.no_req)
            print(output)
            cols = []
            date = strftime("%Y%m%d%H%M", gmtime())
            for x in output:
                msisdn=x[0]
                print(msisdn)
                time.sleep(1)
                cur.callproc('resync_customer.resync_one_msisdn', [msisdn])
        except (KeyboardInterrupt, SystemExit):
            cur.close()
            db.close()
            print("Oracle Connection closed by System Interrupt/Keyboard")
        cur.close()
        db.close()
    # def getClob(self):
    #     con_str = getattr(constants, self.in_env)
    #     ip = con_str["ip"]
    #     port = con_str["port"]
    #     SID = con_str["SID"]
    #     usr = con_str["usr"]
    #     pas = con_str["pass"]
    #     DNS_TNS = cx_Oracle.makedsn(ip, port, SID)
    #     DNS_TNS = cx_Oracle.makedsn(ip, port, SID)
    #     db = cx_Oracle.connect(usr, pas, DNS_TNS)
    #     cur = db.cursor()
    #     clob_text='Query empty result'
    #     clob_query = cur.execute(self.sql)
    #     print(self.sql)
    #     try:
    #         for row in clob_query:
    #             clob_text = (row[0].read())
    #     except:
    #         for row in clob_query:
    #             clob_text = row
    #     cur.close()
    #     db.close()
    #     return clob_text

    def getClob(self):
        con_str = getattr(constants, self.in_env)
        ip = con_str["ip"]
        port = con_str["port"]
        SID = con_str["SID"]
        usr = con_str["usr"]
        pas = con_str["pass"]
        DNS_TNS = cx_Oracle.makedsn(ip, port, SID)
        con = cx_Oracle.connect(usr,pas,DNS_TNS)
        cur = con.cursor()
        clob_text='Query empty result'
        clob_query = cur.execute(self.sql)
        queryResultList = []
        try:
            try:
                for row in clob_query:
                    queryResultList.append((row[0].read()))
                return queryResultList
            except Exception:
                for row in clob_query:
                    queryResultList.append(row)
                return queryResultList  
        finally:
            cur.close()
            con.close()

    def makeDictFactory(self,cursor):
        columnNames = [d[0] for d in cursor.description]
        def createRow(*args):
            return dict(zip(columnNames, args))
        return createRow
        
    def getQueryResult(self):
        con_str = getattr(constants, self.in_env)
        ip = con_str["ip"]
        port = con_str["port"]
        SID = con_str["SID"]
        usr = con_str["usr"]
        pas = con_str["pass"]
        DNS_TNS = cx_Oracle.makedsn(ip, port, SID)
        con = cx_Oracle.connect(usr,pas,DNS_TNS)
        cur = con.cursor()
        clob_text='Query empty result'
        clob_query = cur.execute(self.sql)
        queryResultList = []
        cur.rowfactory = self.makeDictFactory(cur)
        return cur.fetchall()

    def insert(self):
        con_str = getattr(constants, self.in_env)
        ip = con_str["ip"]
        port = con_str["port"]
        SID = con_str["SID"]
        usr = con_str["usr"]
        pas = con_str["pass"]
        DNS_TNS = cx_Oracle.makedsn(ip, port, SID)
        try:
            with cx_Oracle.connect(usr, pas, DNS_TNS) as connection:
                with connection.cursor() as cursor:
                    print(self.fn)
                    print(self.sql)
                    cursor.executemany(self.sql,[self.fn])
                    connection.commit()
                    row_cnt = cursor.rowcount
            return(row_cnt)
        except cx_Oracle.Error as e:
            errorObj, = e.args
            print(errorObj)
            return(errorObj)




