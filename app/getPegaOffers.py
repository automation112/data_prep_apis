import requests
import json, ast
import smtplib
import copy
import bcolors
import pdb

def getOfferName(flowtype, scenarioName_PC=None, isFullOffersDescription=False, inputInstallments=24, tip_serviciu='NA', sub_id=None, env=None):
    if '_precbu' in flowtype.lower():
        return offerSelection_preCBU(flowtype, inputInstallments, isFullOffersDescription, scenarioName_PC, sub_id,
                                     tip_serviciu, env)
    else:
        return offerSelection_CBU(flowtype, inputInstallments, isFullOffersDescription, scenarioName_PC, sub_id,
                                     tip_serviciu, env)

def offerSelection_preCBU(flowtype, inputInstallments, isFullOffersDescription, scenarioName_PC, sub_id, tip_serviciu, env):
    # print(flowtype, scenarioName_PC, isFullOffersDescription, inputInstallments, tip_serviciu, sub_id)

    env_url = getPegaEndpoint(env)
    print(f'{bcolors.WARN}{bcolors.BOLD}Proccess started for endpoint {env_url}{bcolors.ENDC}')
    headers = {'X-SrcSystem': 'EAI', 'X-Transaction-Id': '12345',
               'X-Correlation-Id': 'TestEAI', 'Content-Type': 'text/html; charset=utf-8'}
    offerType = 'dummy'
    options = ['rate', 'simonly', 'serviceonly']
    resultDict = {}
    t_code = 'SERVICES'
    # if 'bundle' in scenarioName_PC.lower():
    #     t_code = 'SERVICES'
    # elif 'd2s' in scenarioName_PC.lower():
    #     t_code = 'SERVICES'
    # elif 's2d' in scenarioName_PC.lower():
    #     t_code = 'SERVICES'
    # elif 'service' in scenarioName_PC.lower():
    #     t_code = 'SERVICES'
    contor = 0
    for i in options:
        if i in scenarioName_PC.lower():
            offerType = i
        else:
            contor += 1
            if contor == 3:
                print(f'WARNING - > BAD KEY, no options {options} in scenarioName_PC -> {scenarioName_PC}')
    inputParams = flowtype + '_' + tip_serviciu + ',' + '_' + 'Scenariul ' + '[' + scenarioName_PC + ']'
    # print(t_code,flowtype)
    request = getTemplate(flow_type=flowtype, scenarioName_PC=scenarioName_PC, tip_serviciu=tip_serviciu, sub_id=sub_id)

    response = requests.post(env_url, json=request, headers=headers)
    unicode_resp = ast.literal_eval(json.dumps(response.json()))
    if 'rate' in offerType.lower():
        offerType = 'installments'
    else:
        offerType = 'no_installments'
    filteredOffersDict = {}
    a = 0
    b = 0
    templist = []
    templist2 = []
    # print(unicode_resp)
    for offerDict in unicode_resp:

        a += 1
        templist.append(offerDict['name'])
        for f in offerDict['parts']['lineItem'][0]['specification']['characteristicsValue']:
            if f['characteristicName'] == 'Commitment':
                commitment = str(f['value'])
            elif f['characteristicName'] == 'OPID':
                opid = str(f['value'])
        for dictionary in (offerDict['parts']['lineItem'][0]['specification']['characteristicsValue']):

            b += 1
            if 'characteristicName' in dictionary.keys():
                # print(dictionary)
                if dictionary['characteristicName'] == 'MobilePromoDiscountValue' and dictionary['value'] == '':
                    filteredOffersDict.update({str(offerDict['name']) + '_' + commitment: {'discount': 'false'}})
                elif dictionary['characteristicName'] == 'MobilePromoDiscountValue' and int(dictionary['value']) > 0:
                    filteredOffersDict.update({str(offerDict['name']) + '_' + commitment: {'discount': 'true'}})

            installments = str(offerDict['parts']['lineItem'][0]['category'][-1]['value'])
            offerName = str(offerDict['parts']['lineItem'][0]['name'])
            offerPrice = str(offerDict['parts']['lineItem'][0]['price']['value'])
            ppOfferId = str(offerDict['parts']['lineItem'][0]['productOffer']['id'][0]['value'])

            for f in offerDict['parts']['lineItem'][1]['specification']['characteristicsValue']:
                if f['characteristicName'] == 'NoOfInstallments':
                    noInstallments = str(f['value'])

        templist2.append(installments)
        filteredOffersDict[str(offerDict['name']) + '_' + commitment].update({'isInstallment': installments,
                                                                              'offerName': offerDict['name'],
                                                                              'ppOfferId': ppOfferId, 'opid': opid,
                                                                              'noInstallments': noInstallments,
                                                                              'offerDetails': str(
                                                                                  offerDict['parts']['lineItem'][0][
                                                                                      'specification'][
                                                                                       'characteristicsValue'])})
    if isFullOffersDescription == True:
        return filteredOffersDict
    else:
        result = targetOffer(filteredOffersDict, offerType, inputInstallments, scenarioName_PC)
    return result, request, offerType, inputParams

def offerSelection_CBU(flowtype, inputInstallments, isFullOffersDescription, scenarioName_PC, sub_id, tip_serviciu, env):
    # print('hereweqewqewqe')
    # print(flowtype, scenarioName_PC, isFullOffersDescription, inputInstallments, tip_serviciu, sub_id)

    env_url = getPegaEndpoint(env)
    print(f'{bcolors.WARN}{bcolors.BOLD}Proccess started for endpoint {env_url}{bcolors.ENDC}')
    headers = {'X-SrcSystem': 'EAI', 'X-Transaction-Id': '12345',
               'X-Correlation-Id': 'TestEAI', 'Content-Type': 'text/html; charset=utf-8'}

    offerType = 'dummy'
    options = ['rate', 'simonly', 'serviceonly']
    resultDict = {}
    t_code = 'SERVICES'
    # if 'bundle' in scenarioName_PC.lower():
    #     t_code = 'SERVICES'
    # elif 'd2s' in scenarioName_PC.lower():
    #     t_code = 'SERVICES'
    # elif 's2d' in scenarioName_PC.lower():
    #     t_code = 'SERVICES'
    # elif 'service' in scenarioName_PC.lower():
    #     t_code = 'SERVICES'
    contor = 0
    for i in options:
        if i in scenarioName_PC.lower():
            offerType = i
        else:
            contor += 1
            if contor == 3:
                print(f'WARNING - > BAD KEY, no options {options} in scenarioName_PC -> {scenarioName_PC}')

    inputParams = flowtype + '_' + tip_serviciu + ',' + '_' + 'Scenariul ' + '[' + scenarioName_PC + ']'
    # print(t_code,flowtype)
    request = getTemplate(flow_type=flowtype, scenarioName_PC=scenarioName_PC, tip_serviciu=tip_serviciu, sub_id=sub_id)

    response = requests.post(env_url, json=request, headers=headers)
    unicode_resp = ast.literal_eval(json.dumps(response.json()))
    # print(unicode_resp)
    if 'rate' in offerType.lower():
        offerType = 'installments'
    else:
        offerType = 'no_installments'
    filteredOffersDict = {}
    cycleCount = 0
    selectionDictionary = {
    'ppOfferId': 'Not_Implemented'
}
    print(unicode_resp)
    for offerDict in unicode_resp:
        # print(offerDict)
        cycleCount += 1
        selectionDictionary['offerName'] = offerDict['name']
        for lineItemElement in offerDict['parts']['lineItem']:
            selectionDictionary = getOfferPrice(lineItemElement, selectionDictionary)
            selectionDictionary = getOfferDiscount_isInstallments(lineItemElement, selectionDictionary)
            for characteristicValueDict in lineItemElement['specification']['characteristicsValue']:
                if type(characteristicValueDict) == dict:
                    # print(characteristicValueDict)
                    if 'characteristicName' in characteristicValueDict.keys():
                        if 'Commitment' in characteristicValueDict['characteristicName']:
                            selectionDictionary['commitment'] = characteristicValueDict['value']
                        if 'NoOfInstallments' in characteristicValueDict['characteristicName']:
                            selectionDictionary['noInstallments'] = characteristicValueDict['value']

                    if 'type' in characteristicValueDict.keys():
                        if 'INSTALLMENT_VALUE' in characteristicValueDict['type']:
                            selectionDictionary['installmentValue'] = characteristicValueDict['value']
        filteredOffersDict[str(selectionDictionary['offerName']) + '_' + selectionDictionary['commitment']] = selectionDictionary

    if isFullOffersDescription == True:
        return filteredOffersDict
    else:
        result = targetOffer(filteredOffersDict, offerType, inputInstallments, scenarioName_PC)
    return result, request, offerType, inputParams

def getPegaEndpoint(env):
    #DEXUAT1
    if env.lower() == 'pega_uat3':
        env_url = 'http://bv-eai-uat5.connex.ro:7080/eai/services/customerMarketingProduct/*'
    #DEXUAT2
    elif env.lower() == 'pega_uat2':
        # env_url = 'http://bv-eai-uat6.connex.ro:7080/eai/services/customerMarketingProduct/*'
        env_url = 'http://10.13.170.36:9080/prweb/PRRestService/customerMarketingProductAPI/v1/customerMarketingProduct/search'
    #DEXUAT3
    elif env.lower() == 'pega_uat1':
        env_url = 'http://bv-eai-uat5.connex.ro:7082/eai/services/customerMarketingProduct/search'
    else:
        print("NO URL SELECTED")
        env_url = "NO URL SELECTED"

    return env_url
#wbuild12321321321532
##1232143
#ewqtrqwe
def getOfferPrice(lineItemElement, selectionDictionary):
    selectionDictionary = copy.deepcopy(selectionDictionary)
    if type(lineItemElement) == dict:
        for lineItmKey, lineItemValue in lineItemElement.items():
            if lineItmKey == 'price' and float(lineItemValue['value']) > 0:
                selectionDictionary['offerPrice'] = lineItemValue['value']
                return selectionDictionary
            else:
                return selectionDictionary

def getOfferDiscount_isInstallments(lineItemElement, selectionDictionary):
    if type(lineItemElement) == dict:
        for key, value in lineItemElement.items():
            selectionDictionary = copy.deepcopy(selectionDictionary)
            if key == 'category':
                # print(type(lineItemElement), lineItemElement)
                for categoryElem in value:
                    if 'Classification' in categoryElem.values():
                        # print(categoryElem['value'])
                        if categoryElem['value'] == 'FC':
                            # print('here')
                            selectionDictionary.update({'discount': 'true'})
                        else:
                            print(selectionDictionary)
                            try:
                                if selectionDictionary['discount'] != 'true':
                                    selectionDictionary.update({'discount': 'false'})
                            except KeyError:
                                selectionDictionary.update({'discount': ''})

                    if 'PriceplanType' in categoryElem.values():
                        # print(selectionDictionary['offerName'],lineItemElement)
                        selectionDictionary.update({'isInstallment': categoryElem['value']})
    # print(selectionDictionary)
    return selectionDictionary

def targetOffer(filterDict, offerType, inputInstallments, scenarioPC):
    result = {}
    # print(offerType)
    # print(filterDict, '@############')
    if 'bundle' in scenarioPC.lower():
        inputInstallments = 1

    for key, value in filterDict.items():
        # print(value)
        # print(value['isInstallment'].lower(), offerType, value['discount'].lower(), int(value['noInstallments']),
        #       inputInstallments, value['offerName'].lower().replace(' ', ''))
        if value['isInstallment'].lower() == offerType == 'installments' and value['discount'].lower() == 'true' and int(value['noInstallments']) >= inputInstallments and 'rate' in value['offerName'].lower():
            result.update({'discount': key})
        elif value['isInstallment'].lower() == offerType == 'installments' and value[
            'discount'].lower() == 'false' and int(value['noInstallments']) >= inputInstallments and 'rate' in value['offerName'].lower():
            result.update({'rate': key})
        elif value['isInstallment'].lower() == offerType == 'no_installments' and value['discount'].lower() == 'true' and 'simo' in value['offerName'].lower().replace(' ', ''):
            result.update({'discount': key})
        elif value['isInstallment'].lower() == offerType == 'no_installments' and value['discount'].lower() == 'false' and 'simo' in value['offerName'].lower().replace(' ', ''):
            result.update({'simonly': key})
    return (result)

def getTemplate(flow_type, scenarioName_PC, tip_serviciu, sub_id):
    if 'bundle' in scenarioName_PC.lower():
        config_oferte = 'BUNDLES'
    elif 'd2s' in scenarioName_PC.lower():
        config_oferte = 'D2S'
    else:
        config_oferte = 'D2S'

    query = f"""select t.request from table_predefined_request t where t.flow_type='{flow_type.upper()}' and t.config_oferte='{config_oferte}' and t.tip_serviciu='{tip_serviciu}'"""

    print(query)

    req_template = requests.get(f"http://devops02.connex.ro:8020/executeQuery/{query}")
    # print(f'----------------------------------\n{req_template.text}\n-----------------------------')
    return ast.literal_eval(req_template.text % {'sub_id': sub_id})

def pegaFilterAnaliser(request, offerType, offerName):
    request['queries'].append({
        "query": f"$.parts.specification.characteristicsValue.characteristicName='FilterPromoName' & $.parts.specification.characteristicsValue.value={offerName.replace('_24', '').replace('_12', '')}",
        "operator": "None"
    })

    env_url = getPegaEndpoint(env)

    headers = {'X-SrcSystem': 'EAI', 'X-Transaction-Id': '12345',
               'X-Correlation-Id': 'TestEAI', 'Content-Type': 'text/html; charset=utf-8'}

    response = requests.post(env_url, json=request, headers=headers)
    unicode_resp = ast.literal_eval(json.dumps(response.json()))

    # print(unicode_resp)

    discTrueContor = 0
    discFalseContor = 0
    templist = []
    filteredOffersDict = {}
    discountTrueLst = []
    # print(unicode_resp, "@@@@@@@@@@@@@@")
    for offerDict in unicode_resp:
        for f in offerDict['parts']['lineItem'][0]['specification']['characteristicsValue']:
            if f['characteristicName'] == 'Commitment':
                commitment = str(f['value'])
            elif f['characteristicName'] == 'OPID':
                opid = str(f['value'])
        for dictionary in (offerDict['parts']['lineItem'][-2]['specification']['characteristicsValue']):

            if 'characteristicName' in dictionary.keys():

                if dictionary['characteristicName'] == 'MobilePromoDiscountValue' and dictionary['value'] == '':
                    filteredOffersDict.update({str(offerDict['name']) + '_' + commitment: {'discount': 'false'}})
                    discFalseContor += 1
                elif dictionary['characteristicName'] == 'MobilePromoDiscountValue' and int(dictionary['value']) > 0:
                    filteredOffersDict.update({str(offerDict['name']) + '_' + commitment: {'discount': 'true'}})
                    discTrueContor += 1
                    discountTrueLst.append(str(offerDict['name']))

    # print(discTrueContor)

    if len(discountTrueLst) > 2 and offerType == 'no_installments':

        return True, discountTrueLst[1:]
    else:
        return False, None

def pegaOffersFixEmail(resultDict, isMultipleOffers, offersToModify, inputParams):
    status = False

    # emailContent = ''

    # emailContent = emailContent + f'\t•Pentru flow-ul -> [{"".join(inputParams).replace("_", " ")}]:'

    discountOffrsDict = {}

    if isMultipleOffers == True and 'simonly' in list(resultDict.keys()):
        # emailContent = emailContent + f' \n\t\t* De scos discount-ul pentru urmatoarele oferte:\n\t\t-> {offersToModify}'
        status = True
        discountOffrsDict.update({inputParams:{'remove': offersToModify}})


    if len(resultDict) < 2:
        # emailContent = emailContent + f'\n\t\t* De adaugat oferta cu : "{str(list(resultDict.keys())[0])}"'
        status = True
        # print(resultDict)
        discountOffrsDict.update({inputParams:{'add': str(list(resultDict.keys())[0])}})

    if status == True:
        pass

    return discountOffrsDict

def createEmailBody(resultDict):

    removedOffersDctList = []
    keysLst = []
    otherOfferDictList = {}

    rmOffersDict = {'common': {},
                    'unique': {}}

    for key, value in resultDict['Result'].items():
        if 'remove' in resultDict['Result'][key].keys() and '_discount' not in key.lower():
            # print(resultDict['Result'][key].keys())
            # print(value)
            removedOffersDctList.append({key:value})
            keysLst.append(key)
        elif 'add' in resultDict['Result'][key].keys():
            otherOfferDictList.update({key:value})


    print('other stuff', otherOfferDictList)

    # print(removedOffersDctList[1])

    # keysLst = list(resultDict['Result'].keys())
    tmpLst = []
    resultfnlLst = []
    removedKeysLst = []
    # print(list(range(0, len(removedOffersDctList))))
    # print(len(removedOffersDctList))
    for j in range(0, len(removedOffersDctList)):
        for i in range(0, len(removedOffersDctList)):
            # print(removedOffersDctList)
            try:
                if i == j and i < len(removedOffersDctList) - 1:
                    k = i + 1
                else: k = i
                #
                # if k == len(removedOffersDctList) - 1:
                #     k = i

                # if k == 5 and j == len(removedOffersDctList) -1 :
                #     print(k)
                #     print(removedOffersDctList[j][str(keysLst[j])]['remove'], '\n---------------------\n', removedOffersDctList[k][str(keysLst[k])]['remove'])
                if removedOffersDctList[j][str(keysLst[j])]['remove'] == removedOffersDctList[k][str(keysLst[k])]['remove']:
                    tmpLst.append(str(keysLst[j]))
                    tmpLst.append(str(keysLst[k]))
                    # print(str(keysLst[j]), s  tr(keysLst[i]))
                    removedKeysLst.append(keysLst[j])
                    removedKeysLst.append(str(keysLst[k]))
                else:
                    removedKeysLst.append(str(keysLst[j]))
            except IndexError:
                print('here', removedOffersDctList[k][str(keysLst[k])]['remove'])
        resultfnlLst.append(list(set(tmpLst)))
        tmpLst = []

    commonList = []
    for i in resultfnlLst:
        i = sorted(i)
        if i not in commonList:
            commonList.append(i)

    commonList = [x for x in commonList if x]

    for i in commonList:
        # print('-------------------',i)
        # print(' | '.join(i))
        rmOffersDict['common'][' | '.join(i)] = resultDict['Result'][str(i[0])]['remove']

    # print(commonOffersDict)

    # print(commonList)

    # print(commonList)
    tmpMergeCommonLst = []

    if len(commonList) > 1:
        for indexElem in range(0, len(commonList)):
            try:
                tmpMergeCommonLst.append(commonList[indexElem] + commonList[indexElem+1])
            except IndexError:
                pass
    else:
        tmpMergeCommonLst = commonList



    # uniqueOffersList = (list((set(resultDict['Result'].keys())) - (set(removedKeysLst))))
    # print(tmpMergeCommonLst[0])
    # print(list(set(removedKeysLst)))
    # print(tmpMergeCommonLst)
    try:
        uniqueOffersList = list(set(removedKeysLst) - set(tmpMergeCommonLst[0]))

        for i in uniqueOffersList:
            rmOffersDict['unique'][i] = resultDict['Result'][str(i)]['remove']
    except IndexError:
        pass

    # print(rmOffersDict)

    emailContent = ''

    for key, value in rmOffersDict.items():
        if key == 'common':
            for commonKey, commonValue in value.items():
                if '|' in commonKey:
                    emailContent = emailContent + f'\n\t\t-> Pentru urmatoarele flow-uri comune: {commonKey}\n\t\t\t* Sa se scoata discount pentru urmatoarele oferte -> {commonValue}'
                    emailContent = emailContent + '\n'
                # emailContent = emailContent + '\n\t\t\t------------------------------------------------------------------'

        elif key == 'unique':
            for uniqueKey, uniqueValue in value.items():
                emailContent = emailContent + f'\n\t\t-> Pentru urmatorul flow: {uniqueKey}\n\t\t\t* Sa se scoata discount pentru urmatoarele oferte -> {uniqueValue}'
                # emailContent = emailContent + '\n\t\t\t------------------------------------------------------------------'
                emailContent = emailContent + '\n'

    # for key, value in
    # print(otherOfferDictList)

    emailContent = otherOffersEmailContent(emailContent, otherOfferDictList)

    return emailContent

def otherOffersEmailContent(emailContent, otherOfferDictList):
    keyLst = list(otherOfferDictList.keys())
    dct = copy.deepcopy(otherOfferDictList)
    otherCommonOfferDictList = {}
    for i in keyLst:

        flowName = i.split(',')[0]
        scenarioName = i.split(',')[1]
        otherCommonOfferDictList[flowName] = {}

        for key, value in dct.items():
            futureFlowName = key.split(',')[0]
            futureScenarioName = key.split(',')[1]
            val = list(value.values())[0]

            if val not in otherCommonOfferDictList[flowName].keys() and dct[i] == value:
                otherCommonOfferDictList[flowName].update({val: []})

            if flowName == futureFlowName and dct[i] == value:
                if val in otherCommonOfferDictList[flowName].keys():
                    otherCommonOfferDictList[flowName][val].append(
                        futureScenarioName.replace("_Scenariul ", "").replace(']', '').replace('[', ''))
    for key, value in otherCommonOfferDictList.items():
        emailContent = emailContent + f'\n\t\t-> Pentru flow-ul de: "{key}"\n\t\t\t* pentru scenariile {list(value.values())[0]} sa se adauge o oferta cu {list(value.keys())[0]}\n'
    return emailContent

def getDictValue(tc_dict=None, inputKey=None):

    convertDict = {'true': True,
                   'false': False,
                   'None': None,
                   'null': None,
                   'inputInstallments': 24,
                   'sendEmail': False,
                   'checkValidFlowOffers': False,
                   'isFullOffersDescription': None,
                   'tip_serviciu':'NA',
                   }


    possibleInputLst = ['true', 'false', 'None', 'null']

    for key, value in tc_dict.items():
        if (type(value) == str) and value.lower() in possibleInputLst:
            tc_dict[key] = convertDict[value.lower()]


    try:
        return tc_dict[inputKey]
    except (KeyError, ValueError) as e:
        if inputKey in convertDict.keys():
            return convertDict[inputKey]
        else:
            return None

def main(input_json):
    print(input_json)
    sendEmail = getDictValue(input_json, 'sendEmail')
    checkValidFlowOffers = getDictValue(input_json, 'checkValidFlowOffers')
    email_TO = getDictValue(input_json, 'email_TO')
    flowtype = getDictValue(input_json, 'flowtype')
    scenarioName_PC = getDictValue(input_json, 'scenarioName_PC')
    isFullOffersDescription = getDictValue(input_json, 'isFullOffersDescription')
    inputInstallments = getDictValue(input_json, 'inputInstallments')
    tip_serviciu = getDictValue(input_json, 'tip_serviciu')
    sub_id = getDictValue(input_json, 'sub_id')
    global env
    env = getDictValue(input_json, 'env')

    content = []
    scenarioPC_List = [
        'BundleOffers_Rate',
        'BundleOffers_Rate_Discout',
        'BundleOffers_SimOnly_si_Discount',
        'BundleOffers_SimOnly',
        'OferteConfigurabile_S2D_Rate',
        'OferteConfigurabile_S2D_Rate_si_Discount',
        'OferteConfigurabile_S2D_SimOnly_si_Discount',
        'OferteConfigurabile_S2D_SimOnly',
        'OferteConfigurabile_ServiceOnly',
        'OferteConfigurabile_ServiceOnly_Discount',
    ]

    stackDict = {'Result': {}}
    emailContent = ''
    contor = 0

    if 'precbu' in flowtype.lower():
        acquisitionFlows = ['GA_PRECBU', 'MNP_PRECBU', 'MFP_PRECBU']
    else:
        acquisitionFlows = ['GA_CBU_OMS', 'MNP_CBU_OMS', 'MFP_CBU_OMS']


    if checkValidFlowOffers:
        for j in acquisitionFlows:
            # print(j)
            # print(j)
            for i in scenarioPC_List:
                contor += 1
                print(f'Quantification in motion -> {contor}')
                if 'MNP' in j:
                    for k in ['PREPAID', 'POSTPAID']:
                        tipSrv = k
                        result_dict, request, offerType, inputParams = getOfferName(flowtype=j, scenarioName_PC=i,
                                                                                    inputInstallments=6,
                                                                                    tip_serviciu=tipSrv,
                                                                                    sub_id=sub_id, env=env)
                        try:
                            status, offersToModifyList = pegaFilterAnaliser(request, offerType,
                                                                            str(result_dict['simonly']))
                        except KeyError:
                            status, offersToModifyList = pegaFilterAnaliser(request, offerType,
                                                                            str(list(result_dict)[0]))


                        dict = pegaOffersFixEmail(result_dict, status, offersToModifyList, inputParams)
                        stackDict['Result'].update(dict)
                        # print(stackDict)
                        # emailContent = emailContent + createEmailBody(stackDict)

                        # print((emailContent))

                else:
                    tipSrv = 'NA'
                    result_dict, request, offerType, inputParams = getOfferName(flowtype=j, scenarioName_PC=i,
                                                                                inputInstallments=6, tip_serviciu=tipSrv,
                                                                                sub_id=sub_id, env=env)
                    print(result_dict)


                    if(len(result_dict)) == 0:
                        # print("#@@@@@@@@@@@@@@@@@@@", result_dict, j, i, 36, tipSrv, sub_id)
                        pass

                    try:
                        print('@@@@@@@@@@@@@@@@@@@', result_dict, '\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
                        status, offersToModifyList = pegaFilterAnaliser(request, offerType, str(result_dict['simonly']))
                    except KeyError:
                        print('@@@@@@@@@@@@@@@@@@@', result_dict, '\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')

                        status, offersToModifyList = pegaFilterAnaliser(request, offerType, str(list(result_dict.values())[0]))


                    dict = pegaOffersFixEmail(result_dict, status, offersToModifyList, inputParams)
                    stackDict['Result'].update(dict)
                    print(stackDict)

                    # emailContent = emailContent + createEmailBody(stackDict)

        emailContent = emailContent + createEmailBody(stackDict)
        # emailContent = f'Salut,\n\n\tVa rog sa ne ajutati cu modificarile de mai jos:\n\tDevice id folosit (Samsung S20, 5G) = TSAS205GN\n{emailContent}\n\n*Email-ul a fost generat automat*\n\nMultumim,\nDL-T-VOIS-VFRO-TESTING-AUTOMATION'

        if 'precbu' in input_json['flowtype']:
            emailContent = f'Salut,\n\n\tVa rog sa ne ajutati cu modificarile de mai jos:\n\tEnvironment: {env}\n\tDevice id folosit (Samsung S20, 5G) = TSAS205GN\n{emailContent}\n\n*Email-ul a fost generat automat*\n\nMultumim,\nDL-T-VOIS-VFRO-TESTING-AUTOMATION'
            emailContent = emailContent.replace('NA', '').replace(' "discount"',
                                                                  '"discount + optiunea de 36 rate"').replace('_', ' ')
        else:
            emailContent = f'Salut,\n\n\tVa rog sa ne ajutati cu modificarile de mai jos:\n\tEnvironment: {env}\n\tDevice id folosit (Huawei P30 Negru) = THU30P128N\n{emailContent}\n\n*Email-ul a fost generat automat*\n\nMultumim,\nDL-T-VOIS-VFRO-TESTING-AUTOMATION'
            emailContent = emailContent.replace('NA', '').replace('_', ' ')

        print(emailContent.replace('OMS', 'Client nou (to OMS [CRM IND = TRUE])'))

        if sendEmail == True and len(stackDict['Result']) > 0:
            sendEmailMethod(emailContent, email_TO)

        return {'Email Content': emailContent.replace('OMS', 'Client nou (to OMS - [CRM IND = TRUE])')}

    else:

        result_dict, request, offerType, inputParams = getOfferName(flowtype=flowtype, scenarioName_PC=scenarioName_PC,
                                                                    inputInstallments=inputInstallments,
                                                                    tip_serviciu=tip_serviciu,
                                                                    sub_id=sub_id, env=env)
        print(result_dict)
        return result_dict


def sendEmailMethod(emailContent, email_to):
    SERVER = "smtp.connex.ro"
    FROM = "dltvoisvfrotestingautomation@vodafone.com"
    # FROM = 'gabriel.munteanu2@vodafone.com'
    TO = email_to.split(',').append(FROM)
    MSG = f"From: {FROM}\nCC: {FROM}\nTo: {email_to}\nSubject: Pega Offers Sync\n\n{emailContent}"
    server = smtplib.SMTP(SERVER)
    server.sendmail(FROM, TO, MSG)
    server.quit()
    print("Email Send")


if __name__ == '__main__':

    input_json = {'flowtype': 'GA_precbu', 'scenarioName_PC': 'OferteConfigurabile_S2D_SimOnly_si_Discount', 'isFullOffersDescription': 'false', 'inputInstallments': 36, 'tip_serviciu': 'NA', 'sub_id': 717738411, 'env': 'pega_uat1'}
    input_json = {
    "sendEmail": "false",
    "checkValidFlowOffers": "true",
   "flowtype":"GA_precbu",
   "scenarioName_PC":"OferteConfigurabile_D2S_Rate",
   "isFullOffersDescription":"false",
   "inputInstallments":1,
   "tip_serviciu":"NA",
   "sub_id":717738411,
   "env":"pega_uat2",
   "email_TO": "DL-pega_testing@internal.vodafone.com"
}

    input_json = {'flowtype': 'GA_precbu', 'scenarioName_PC': 'OferteConfigurabile_S2D_SimOnly_si_Discount', 'isFullOffersDescription': 'false', 'inputInstallments': 36, 'tip_serviciu': 'NA', 'sub_id': 717738411, 'env': 'pega_uat2'}
    # input_json = {'flowtype': 'GA_cbu_oms', 'scenarioName_PC': 'BundleOffers_SimOnly', 'isFullOffersDescription': 'false', 'inputInstallments': 0, 'tip_serviciu': 'NA', 'sub_id': 2951104439671, 'env': 'pega_uat3'}
    # input_json = {'flowtype': 'GA_cbu_oms', 'scenarioName_PC': 'BundleOffers_SimOnly', 'isFullOffersDescription': 'false', 'inputInstallments': 0, 'tip_serviciu': 'NA', 'sub_id': 2951104439671, 'env': 'pega_uat3'}
    # input_json = {'flowtype': 'MGA_precbu', 'scenarioName_PC': 'OferteConfigurabile_S2D_SimOnly_si_Discount', 'isFullOffersDescription': 'false', 'inputInstallments': 36, 'tip_serviciu': 'PREPAID', 'sub_id': 717738411, 'env': 'pega_uat2'}
    #devops02:8080
    #test
    input_json = {
   "flowtype":"precbu",
   "isFullOffersDescription":"false",
   "inputInstallments":36,
   "tip_serviciu":"NA",
   "sub_id":717738411,
   "env":"pega_uat2",
   "checkValidFlowOffers":"true"
}
    main(input_json)
    # sendEmailMethod(emailContent='test', email_to='gabriel.munteanu2@vodafone.com, iulian.ciuciu@vodafone.com')